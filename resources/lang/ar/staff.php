<?php
	return [
		'staff'=>'طاقم عمل',
		'name'=>'الاسم',
		'details'=>'التفاصيل',
		'image'=>'الصورة',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
