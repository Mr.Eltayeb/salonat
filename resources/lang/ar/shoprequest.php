<?php
	return [
		'shoprequest'=>'طلبات المتجر',
		'item_quantity'=>'كمية المنتج المطلوب',
		'item_totel_price'=>'السعر الكلي للمنتج المطلوب',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
