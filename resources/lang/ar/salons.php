<?php
	return [
		'salons'=>'كوفير',
		'name'=>'الاسم',
		'details'=>'التفاصيل',
		'image'=>'الصورة',
		'lat'=>'خط الطول',
		'lng'=>'خط العرض',
		'Address'=>'العنوان',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
		'logo'=>'لوغو',
		'photo'=>'الصورة',
		'photo_two'=>'الصورة الثانية',
		'photo_three'=>'الصورة الثالثة',
	];
