<?php
	return [
		'shopitem'=>'محتوي المتجر',
		'name'=>'الاسم',
		'price'=>'السعر',
		'quantity'=>'الكمية المتوفرة',
		'description'=>'وصف المنتج',
		'image'=>'الصورة الاولة',
		'image_two'=>'الصورة الثانية',
		'image_three'=>'الصورة الثالثة',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
