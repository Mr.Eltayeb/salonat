<?php
	return [
		'serveries'=>'الخدمات',
		'name'=>'الاسم',
		'type'=>'النوع',
		'price'=>'السعر',
		'time'=>'الزمن',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
