<?php
	return [
		'booking'=>'حجز',
		'time'=>'الزمن',
		'date'=>'اليوم',
		'feedback'=>'معلومات اضافية',
		'status'=>'الحالة',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
