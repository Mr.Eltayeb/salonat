<?php
	return [
		'shopitem'=>'Shop Item',
		'name'=>'name',
		'price'=>'price',
		'quantity'=>'quantity',
		'description'=>'description',
		'image'=>'image',
		'image_two'=>'image two',
		'image_three'=>'image three',
		'edit'=>'edit',
		'show'=>'show',
		'delete'=>'delete',
	];
