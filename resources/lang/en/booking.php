<?php
	return [
		'booking'=>'booking',
		'time'=>'time',
		'date'=>'date',
		'feedback'=>'feedback',
		'status'=>'status',
		'edit'=>'edit',
		'show'=>'show',
		'delete'=>'delete',
	];
