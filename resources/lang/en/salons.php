<?php
	return [
		'salons'=>'salons',
		'name'=>'name',
		'details'=>'details',
		'image'=>'image',
		'lat'=>'lat',
		'lng'=>'lng',
		'Address'=>'Address',
		'edit'=>'edit',
		'show'=>'show',
		'delete'=>'delete',
		'logo'=>'logo',
		'photo'=>'photo one',
		'photo_two'=>'photo Two',
		'photo_three'=>'photo three',
	];
