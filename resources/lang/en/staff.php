<?php
	return [
		'staff'=>'staff',
		'name'=>'name',
		'details'=>'details',
		'image'=>'image',
		'edit'=>'edit',
		'show'=>'show',
		'delete'=>'delete',
	];
