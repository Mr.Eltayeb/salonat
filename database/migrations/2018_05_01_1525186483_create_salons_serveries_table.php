<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonsserveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("serveries", "salons_id"))
		{
	Schema::table("serveries", function (Blueprint $table)  {
		$table->integer("salons_id")->unsigned();
		$table->foreign("salons_id")->references("id")->on("salons")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("serveries", "salons_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("serveries");
			Schema::table("serveries", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("serveries_salons_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("serveries_salons_id_foreign");
					$table->dropColumn("salons_id");
				}else{
					$table->dropColumn("salons_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
