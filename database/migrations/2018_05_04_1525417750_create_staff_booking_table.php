<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffbookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("booking", "staff_id"))
		{
	Schema::table("booking", function (Blueprint $table)  {
		$table->integer("staff_id")->unsigned();
		$table->foreign("staff_id")->references("id")->on("staff")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("booking", "staff_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("booking");
			Schema::table("booking", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("booking_staff_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("booking_staff_id_foreign");
					$table->dropColumn("staff_id");
				}else{
					$table->dropColumn("staff_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
