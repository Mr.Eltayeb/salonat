<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopitemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopitem', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name")->nullable();
			$table->double("price")->nullable();
			$table->integer("quantity")->nullable();
			$table->text("description")->nullable();
			$table->string("image")->nullable();
			$table->string("image_two")->nullable();
			$table->string("image_three")->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopitem');
    }
}
