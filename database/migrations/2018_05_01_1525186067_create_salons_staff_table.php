<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonsstaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("staff", "salons_id"))
		{
	Schema::table("staff", function (Blueprint $table)  {
		$table->integer("salons_id")->unsigned();
		$table->foreign("salons_id")->references("id")->on("salons")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("staff", "salons_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("staff");
			Schema::table("staff", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("staff_salons_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("staff_salons_id_foreign");
					$table->dropColumn("salons_id");
				}else{
					$table->dropColumn("salons_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
