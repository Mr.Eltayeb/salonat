<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserbookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("booking", "user_id"))
		{
	Schema::table("booking", function (Blueprint $table)  {
		$table->integer("user_id")->unsigned();
		$table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("booking", "user_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("booking");
			Schema::table("booking", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("booking_user_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("booking_user_id_foreign");
					$table->dropColumn("user_id");
				}else{
					$table->dropColumn("user_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
