<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonsbookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("booking", "salons_id"))
		{
	Schema::table("booking", function (Blueprint $table)  {
		$table->integer("salons_id")->unsigned();
		$table->foreign("salons_id")->references("id")->on("salons")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("booking", "salons_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("booking");
			Schema::table("booking", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("booking_salons_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("booking_salons_id_foreign");
					$table->dropColumn("salons_id");
				}else{
					$table->dropColumn("salons_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
