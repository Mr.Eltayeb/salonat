<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopitemshoprequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("shoprequest", "shopitem_id"))
		{
	Schema::table("shoprequest", function (Blueprint $table)  {
		$table->integer("shopitem_id")->unsigned();
		$table->foreign("shopitem_id")->references("id")->on("shopitem")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("shoprequest", "shopitem_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("shoprequest");
			Schema::table("shoprequest", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("shoprequest_shopitem_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("shoprequest_shopitem_id_foreign");
					$table->dropColumn("shopitem_id");
				}else{
					$table->dropColumn("shopitem_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
