<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServeriesbookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    		if (!Schema::hasColumn("booking", "serveries_id"))
		{
	Schema::table("booking", function (Blueprint $table)  {
		$table->integer("serveries_id")->unsigned();
		$table->foreign("serveries_id")->references("id")->on("serveries")->onDelete("cascade");

	});		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::disableForeignKeyConstraints();
		if (Schema::hasColumn("booking", "serveries_id"))
		{
			$arrayOfKeys = $this->listTableForeignKeys("booking");
			Schema::table("booking", function ($table) use ($arrayOfKeys) {
			Schema::disableForeignKeyConstraints();
				if(in_array("booking_serveries_id_foreign" , $arrayOfKeys)){
					$table->dropForeign("booking_serveries_id_foreign");
					$table->dropColumn("serveries_id");
				}else{
					$table->dropColumn("serveries_id");
				}
			Schema::enableForeignKeyConstraints();
			});
		}
		Schema::enableForeignKeyConstraints();

    }
}
