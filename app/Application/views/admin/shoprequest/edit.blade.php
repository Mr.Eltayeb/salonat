@extends(layoutExtend())
 @section('title')
    {{ trans('shoprequest.shoprequest') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
 @section('content')
    @component(layoutForm() , ['title' => trans('shoprequest.shoprequest') , 'model' => 'shoprequest' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
         @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/shoprequest/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.shoprequest.relation.user.edit")
            @include("admin.shoprequest.relation.shopitem.edit")
     <div class="form-group {{ $errors->has("item_quantity") ? "has-error" : "" }}" > 
   <label for="item_quantity">{{ trans("shoprequest.item_quantity")}}</label>
    <input type="text" name="item_quantity" class="form-control" id="item_quantity" value="{{ isset($item->item_quantity) ? $item->item_quantity : old("item_quantity") }}"  placeholder="{{ trans("shoprequest.item_quantity")}}">
  </div>
   @if ($errors->has("item_quantity"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("item_quantity") }}</strong>
     </span>
    </div>
   @endif
   <div class="form-group {{ $errors->has("item_totel_price") ? "has-error" : "" }}" > 
   <label for="item_totel_price">{{ trans("shoprequest.item_totel_price")}}</label>
    <input type="text" name="item_totel_price" class="form-control" id="item_totel_price" value="{{ isset($item->item_totel_price) ? $item->item_totel_price : old("item_totel_price") }}"  placeholder="{{ trans("shoprequest.item_totel_price")}}">
  </div>
   @if ($errors->has("item_totel_price"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("item_totel_price") }}</strong>
     </span>
    </div>
   @endif
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('shoprequest.shoprequest') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
