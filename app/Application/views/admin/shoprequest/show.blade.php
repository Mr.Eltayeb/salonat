@extends(layoutExtend())
  @section('title')
    {{ trans('shoprequest.shoprequest') }} {{ trans('home.view') }}
@endsection
  @section('content')
    @component(layoutForm() , ['title' => trans('shoprequest.shoprequest') , 'model' => 'shoprequest' , 'action' => trans('home.view')  ])
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("shoprequest.item_quantity") }}</th>
     <td>{{ nl2br($item->item_quantity) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("shoprequest.item_totel_price") }}</th>
     <td>{{ nl2br($item->item_totel_price) }}</td>
    </tr>
  </table>
          @include('admin.shoprequest.buttons.delete' , ['id' => $item->id])
        @include('admin.shoprequest.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
