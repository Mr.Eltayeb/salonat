@extends(layoutExtend())

@section('title')
    {{ trans('shopitem.shopitem') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('shopitem.shopitem') , 'model' => 'shopitem' , 'action' => trans('home.view')  ])
        <table class="table table-bordered  table-striped">
            <tr>
                <th width="200">{{ trans("shopitem.name") }}</th>
                <td>{{ nl2br($item->name) }}</td>
            </tr>
            <tr>
                <th width="200">{{ trans("shopitem.price") }}</th>
                <td>{{ nl2br($item->price) }}</td>
            </tr>
            <tr>
                <th width="200">{{ trans("shopitem.quantity") }}</th>
                <td>{{ nl2br($item->quantity) }}</td>
            </tr>
            <tr>
                <th width="200">{{ trans("shopitem.description") }}</th>
                <td>{{ nl2br($item->description) }}</td>
            </tr>
            <tr>
                <th width="200">{{ trans("shopitem.image") }}</th>
                <td>
                    <img src="{{ small($item->image) }}" width="100"/>
                </td>
            </tr>
            <tr>
                <th width="200">{{ trans("shopitem.image_two") }}</th>
                <td>
                    <img src="{{ small($item->image_two) }}" width="100"/>
                </td>
            </tr>
            <tr>
                <th width="200">{{ trans("shopitem.image_three") }}</th>
                <td>
                    <img src="{{ small($item->image_three) }}" width="100"/>
                </td>
            </tr>
        </table>

        @include('admin.shopitem.buttons.delete' , ['id' => $item->id])
        @include('admin.shopitem.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
