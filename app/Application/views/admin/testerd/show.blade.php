@extends(layoutExtend())
  @section('title')
    {{ trans('testerd.testerd') }} {{ trans('home.view') }}
@endsection
  @section('content')
    @component(layoutForm() , ['title' => trans('testerd.testerd') , 'model' => 'testerd' , 'action' => trans('home.view')  ])
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("testerd.title") }}</th>
     <td>{{ nl2br($item->title) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("testerd.image") }}</th>
     <td>
     @isset($item)
      @if(json_decode($item->image))
       <input type="hidden" name="oldFiles_image" value="{{ $item->image }}">
       @php $files = returnFilesImages($item , "image"); @endphp
       <div class="row text-center">
       @foreach($files["image"] as $jsonimage )
        <div class="col-lg-2 text-center"><img src="{{ small($jsonimage) }}" width="100"  /><br>
        <span class="btn btn-danger" onclick="deleteThisItem(this)" data-link="{{ url("deleteFile/testerd/".$item->id."?name=".$jsonimage."&filed_name=image") }}"><i class="fa fa-trash"></i></span></div>
       @endforeach
       </div>
       <div class="row text-center">
       @foreach($files["file"] as $jsonimage )
        <div class="col-lg-2 text-center"><a href="{{ url(env("UPLOAD_PATH")."/".$jsonimage) }}" ><i class="fa fa-file"></i></a>
        <span  onclick="deleteThisItem(this)" data-link="{{ url("deleteFile/testerd/".$item->id."?name=".$jsonimage."&filed_name=image") }}"><i class="fa fa-trash"></i> {{ $jsonimage }} </span></div>
       @endforeach
     </div>
      @endif
     @endisset
     </td>
    </tr>
  </table>
  @include("admin.testerd.rate.rate")
          @include('admin.testerd.buttons.delete' , ['id' => $item->id])
        @include('admin.testerd.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
