@extends(layoutExtend())
 @section('title')
    {{ trans('testerd.testerd') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
 @section('content')
    @component(layoutForm() , ['title' => trans('testerd.testerd') , 'model' => 'testerd' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
         @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/testerd/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
     <div class="form-group {{ $errors->has("title") ? "has-error" : "" }}" > 
   <label for="title">{{ trans("testerd.title")}}</label>
    <input type="text" name="title" class="form-control" id="title" value="{{ isset($item->title) ? $item->title : old("title") }}"  placeholder="{{ trans("testerd.title")}}">
  </div>
   @if ($errors->has("title"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("title") }}</strong>
     </span>
    </div>
   @endif
   <div class="form-group {{ $errors->has("image[]") ? "has-error" : "" }}" > 
   <label for="image">{{ trans("testerd.image")}}</label>
    <div id="laraflat-image">
     @isset($item)
      @if(json_decode($item->image))
       <input type="hidden" name="oldFiles_image" value="{{ $item->image }}">
       @php $files = returnFilesImages($item , "image"); @endphp
       <div class="row text-center">
       @foreach($files["image"] as $jsonimage )
        <div class="col-lg-2 text-center"><img src="{{ small($jsonimage) }}" class="img-responsive" /><br>
        <a class="btn btn-danger" onclick="deleteThisItem(this)" data-link="{{ url("deleteFile/testerd/".$item->id."?name=".$jsonimage."&filed_name=image") }}"><i class="fa fa-trash"></i></a></div>
       @endforeach
       </div>
       <div class="row text-center">
       @foreach($files["file"] as $jsonimage )
        <div class="col-lg-2 text-center"><a href="{{ url(env("UPLOAD_PATH")."/".$jsonimage) }}" ><i class="fa fa-file"></i></a>
        <span  onclick="deleteThisItem(this)" data-link="{{ url("deleteFile/testerd/".$item->id."?name=".$jsonimage."&filed_name=image") }}"><i class="fa fa-trash"></i> {{ $jsonimage }} </span></div>
       @endforeach
     </div>
      @endif
     @endisset
      <input name="image[]"  type="file" multiple >
     </div>
  </div>
   @if ($errors->has("image[]"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("image[]") }}</strong>
     </span>
    </div>
   @endif
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('testerd.testerd') }}
                </button>
            </div>
        </form>
        @include("admin.testerd.rate.rate")
    @endcomponent
@endsection
