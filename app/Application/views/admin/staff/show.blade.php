@extends(layoutExtend())
  @section('title')
    {{ trans('staff.staff') }} {{ trans('home.view') }}
@endsection
  @section('content')
    @component(layoutForm() , ['title' => trans('staff.staff') , 'model' => 'staff' , 'action' => trans('home.view')  ])
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("staff.name") }}</th>
     <td>{{ nl2br($item->name) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("staff.details") }}</th>
     <td>{{ nl2br($item->details) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("staff.image") }}</th>
     <td>
     <img src="{{ small($item->image) }}" width="100" />
     </td>
    </tr>
  </table>
          @include('admin.staff.buttons.delete' , ['id' => $item->id])
        @include('admin.staff.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
