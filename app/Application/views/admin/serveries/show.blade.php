@extends(layoutExtend())
  @section('title')
    {{ trans('serveries.serveries') }} {{ trans('home.view') }}
@endsection
  @section('content')
    @component(layoutForm() , ['title' => trans('serveries.serveries') , 'model' => 'serveries' , 'action' => trans('home.view')  ])
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("serveries.name") }}</th>
     <td>{{ nl2br($item->name) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("serveries.type") }}</th>
     <td>{{ nl2br($item->type) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("serveries.price") }}</th>
     <td>{{ nl2br($item->price) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("serveries.time") }}</th>
     <td>{{ nl2br($item->time) }}</td>
    </tr>
  </table>
          @include('admin.serveries.buttons.delete' , ['id' => $item->id])
        @include('admin.serveries.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
