@extends(layoutExtend())
@section('title')
    {{ trans('serveries.serveries') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('serveries.serveries') , 'model' => 'serveries' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/serveries/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.serveries.relation.salons.edit")
            <div class="form-group {{ $errors->has("name") ? "has-error" : "" }}">
                <label for="name">{{ trans("serveries.name")}}</label>
                <input type="text" name="name" class="form-control" id="name"
                       value="{{ isset($item->name) ? $item->name : old("name") }}"
                       placeholder="{{ trans("serveries.name")}}">
            </div>
            @if ($errors->has("name"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("name") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group {{ $errors->has("type") ? "has-error" : "" }}">
                <label for="type">{{ trans("serveries.type")}}</label>
                <input type="text" name="type" class="form-control" id="type"
                       value="{{ isset($item->type) ? $item->type : old("type") }}"
                       placeholder="{{ trans("serveries.type")}}">
            </div>
            @if ($errors->has("type"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("type") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group {{ $errors->has("price") ? "has-error" : "" }}">
                <label for="price">{{ trans("serveries.price")}}</label>
                <input type="text" name="price" class="form-control" id="price"
                       value="{{ isset($item->price) ? $item->price : old("price") }}"
                       placeholder="{{ trans("serveries.price")}}">
            </div>
            @if ($errors->has("price"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("price") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group {{ $errors->has("time") ? "has-error" : "" }}">
                <label for="time">{{ trans("serveries.time")}}</label>
                <input type="text" name="time" class="form-control" id="time"
                       value="{{ isset($item->time) ? $item->time : old("time") }}"
                       placeholder="{{ trans("serveries.time")}}">
            </div>
            @if ($errors->has("time"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("time") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('serveries.serveries') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
