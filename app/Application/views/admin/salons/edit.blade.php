@extends(layoutExtend())
@section('title')
    {{ trans('salons.salons') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('salons.salons') , 'model' => 'salons' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/salons/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.salons.relation.user.edit")
            <div class="form-group {{ $errors->has("name") ? "has-error" : "" }}">
                <label for="name">{{ trans("salons.name")}}</label>
                <input type="text" name="name" class="form-control" id="name"
                       value="{{ isset($item->name) ? $item->name : old("name") }}"
                       placeholder="{{ trans("salons.name")}}">
            </div>
            @if ($errors->has("name"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("name") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group {{ $errors->has("details") ? "has-error" : "" }}">
                <label for="details">{{ trans("salons.details")}}</label>
                <input type="text" name="details" class="form-control" id="details"
                       value="{{ isset($item->details) ? $item->details : old("details") }}"
                       placeholder="{{ trans("salons.details")}}">
            </div>
            @if ($errors->has("details"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("details") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group {{ $errors->has("image") ? "has-error" : "" }}">
                <label for="image">{{ trans("salons.image")}}</label>
                @if(isset($item) && $item->image != "")
                    <br>
                    <img src="{{ small($item->image) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="image">
            </div>
            @if ($errors->has("image"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("image") }}</strong>
     </span>
                </div>
            @endif


            <div class="form-group {{ $errors->has("logo") ? "has-error" : "" }}">
                <label for="image">{{ trans("salons.logo")}}</label>
                @if(isset($item) && $item->logo != "")
                    <br>
                    <img src="{{ small($item->logo) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="logo">
            </div>
            @if ($errors->has("logo"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("logo") }}</strong>
     </span>
                </div>
            @endif



            {{--//photo--}}

            <div class="form-group {{ $errors->has("photo") ? "has-error" : "" }}">
                <label for="photo">{{ trans("salons.photo")}}</label>
                @if(isset($item) && $item->photo != "")
                    <br>
                    <img src="{{ small($item->photo) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="photo">
            </div>
            @if ($errors->has("photo"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("photo") }}</strong>
     </span>
                </div>
            @endif

            <div class="form-group {{ $errors->has("photo_two") ? "has-error" : "" }}">
                <label for="photo_two">{{ trans("salons.photo_two")}}</label>
                @if(isset($item) && $item->photo_two != "")
                    <br>
                    <img src="{{ small($item->photo_two) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="photo_two">
            </div>
            @if ($errors->has("photo_two"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("photo_two") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group {{ $errors->has("photo_three") ? "has-error" : "" }}">
                <label for="photo_three">{{ trans("salons.photo_three")}}</label>
                @if(isset($item) && $item->photo_three != "")
                    <br>
                    <img src="{{ small($item->photo_three) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="photo_three">
            </div>
            @if ($errors->has("photo_three"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("photo_three") }}</strong>
     </span>
                </div>
            @endif




            {{--End of Photo--}}


            <div class="form-group {{ $errors->has("lat") ? "has-error" : "" }}">
                <label for="lat">{{ trans("salons.lat")}}</label>
                <input type="text" name="lat" class="form-control lat" style="display:none" id="lat"
                       value="{{ isset($item->lat) ? $item->lat : old("lat") }}" placeholder="{{ trans("salons.lat")}}">
                <div class="pac-card" id="pac-card">
                    <div>
                        <div id="title">
                            {{ trans("admin.Autocomplete search") }}
                        </div>
                        <div id="type-selector" class="pac-controls">
                            <input type="radio" name="type" id="changetype-all" checked="checked">
                            <label for="changetype-all">{{ trans("admin.All") }}</label>
                            <input type="radio" name="type" id="changetype-establishment">
                            <label for="changetype-establishment">{{ trans("admin.Establishments") }}</label>
                            <input type="radio" name="type" id="changetype-address">
                            <label for="changetype-address">{{ trans("admin.Addresses") }}</label>
                            <input type="radio" name="type" id="changetype-geocode">
                            <label for="changetype-geocode">{{ trans("admin.Geocodes") }}</label>
                        </div>
                        <div id="strict-bounds-selector" class="pac-controls">
                            <input type="checkbox" id="use-strict-bounds" value="">
                            <label for="use-strict-bounds"> {{ trans("admin.Strict Bounds") }} </label>
                        </div>
                    </div>
                    <div id="pac-container">
                        <input id="pac-input" type="text" placeholder="{{ trans("admin.Enter a location") }}">
                    </div>
                </div>
                <div id="map" style="width: 100%;height: 500px;"></div>
                <div id="infowindow-content">
                    <img src="" width="16" height="16" id="place-icon">
                    <span id="place-name" class="title"></span><br>
                    <span id="place-address"></span>
                </div>
            </div>
            @if ($errors->has("lat"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("lat") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group {{ $errors->has("lng") ? "has-error" : "" }}">
                <label for="lng">{{ trans("salons.lng")}}</label>
                <input type="text" name="lng" class="form-control lng" style="display:none" id="lng"
                       value="{{ isset($item->lng) ? $item->lng : old("lng") }}" placeholder="{{ trans("salons.lng")}}">
            </div>
            @if ($errors->has("lng"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("lng") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group {{ $errors->has("Address") ? "has-error" : "" }}">
                <label for="Address">{{ trans("salons.Address")}}</label>
                <input type="text" name="Address" class="form-control" id="Address"
                       value="{{ isset($item->Address) ? $item->Address : old("Address") }}"
                       placeholder="{{ trans("salons.Address")}}">
            </div>
            @if ($errors->has("Address"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("Address") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('salons.salons') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
