@extends(layoutExtend())
@section('title')
    {{ trans('booking.booking') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('booking.booking') , 'model' => 'booking' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/booking/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.booking.relation.staff.edit")
            @include("admin.booking.relation.serveries.edit")
            @include("admin.booking.relation.user.edit")
            @include("admin.booking.relation.salons.edit")
            <div class="form-group {{ $errors->has("time") ? "has-error" : "" }}">
                <label for="time">{{ trans("booking.time")}}</label>
                <input type="text" name="time" class="form-control time" id="time"
                       value="{{ isset($item->time) ? $item->time : old("time") }}"
                       placeholder="{{ trans("booking.time")}}">
            </div>
            @if ($errors->has("time"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("time") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group {{ $errors->has("date") ? "has-error" : "" }}">
                <label for="date">{{ trans("booking.date")}}</label>
                <input type="text" name="date" class="form-control datepicker2" id="date"
                       value="{{ isset($item->date) ? $item->date : old("date") }}"
                       placeholder="{{ trans("booking.date")}}">
            </div>
            @if ($errors->has("date"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("date") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group {{ $errors->has("feedback") ? "has-error" : "" }}">
                <label for="feedback">{{ trans("booking.feedback")}}</label>
                <input type="text" name="feedback" class="form-control" id="feedback"
                       value="{{ isset($item->feedback) ? $item->feedback : old("feedback") }}"
                       placeholder="{{ trans("booking.feedback")}}">
            </div>
            @if ($errors->has("feedback"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("feedback") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group {{ $errors->has("status") ? "has-error" : "" }}">
                <label for="status">{{ trans("booking.status")}}</label>
                <input type="text" name="status" class="form-control" id="status"
                       value="{{ isset($item->status) ? $item->status : old("status") }}"
                       placeholder="{{ trans("booking.status")}}">
            </div>
            @if ($errors->has("status"))
                <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("status") }}</strong>
     </span>
                </div>
            @endif
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('booking.booking') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
