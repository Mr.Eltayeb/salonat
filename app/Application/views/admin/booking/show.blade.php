@extends(layoutExtend())
  @section('title')
    {{ trans('booking.booking') }} {{ trans('home.view') }}
@endsection
  @section('content')
    @component(layoutForm() , ['title' => trans('booking.booking') , 'model' => 'booking' , 'action' => trans('home.view')  ])
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("booking.time") }}</th>
     <td>{{ nl2br($item->time) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("booking.date") }}</th>
     <td>{{ nl2br($item->date) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("booking.feedback") }}</th>
     <td>{{ nl2br($item->feedback) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("booking.status") }}</th>
     <td>{{ nl2br($item->status) }}</td>
    </tr>
  </table>
          @include('admin.booking.buttons.delete' , ['id' => $item->id])
        @include('admin.booking.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
