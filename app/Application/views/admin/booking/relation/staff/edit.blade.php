		<div class="form-group {{ $errors->has("staff") ? "has-error" : "" }}">
			<label for="staff">{{ trans( "staff.staff") }}</label>
			@php $staff = App\Application\Model\Staff::pluck("name" ,"id")->all()  @endphp
			@php  $staff_id = isset($item) ? $item->staff_id : null @endphp
			<select name="staff_id"  class="form-control" >
			@foreach( $staff as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $staff_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
			@endforeach
			</select>
			@if ($errors->has("staff"))
				<div class="alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first("staff") }}</strong>
					</span>
				</div>
			@endif
			</div>
