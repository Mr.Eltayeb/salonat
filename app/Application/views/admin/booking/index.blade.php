@extends(layoutExtend())

@section('title')
     {{ trans('booking.booking') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@push('header')
    <button class="btn btn-danger" onclick="deleteThemAll(this)" data-link="{{ url('admin/booking/pluck') }}" ><i class="fa fa-trash"></i></button>
    <button class="btn btn-success" onclick="checkAll(this)"  ><i class="fa fa-check-circle-o"></i> </button>
    <button class="btn btn-warning" onclick="unCheckAll(this)"  ><i class="fa fa-check-circle"></i> </button>
@endpush

@push('search')
    <form method="get" class="form-inline">
        <div class="form-group">
            <input type="text" name="from" class="form-control datepicker2" placeholder="{{ trans('admin.from') }}" value="{{ request()->has('from') ? request()->get('from') : '' }}">
        </div>
        <div class="form-group">
            <input type="text" name="to" class="form-control datepicker2" placeholder="{{ trans('admin.to') }}" value="{{ request()->has('to') ? request()->get('to') : '' }}">
        </div>
		<div class="form-group">
			<input type="text" name="time" class="form-control time" placeholder="{{ trans("booking.time") }}" value="{{ request()->has("time") ? request()->get("time") : "" }}">
		</div>
		<div class="form-group">
			<input type="text" name="date" class="form-control datepicker2" placeholder="{{ trans("booking.date") }}" value="{{ request()->has("date") ? request()->get("date") : "" }}">
		</div>
		<div class="form-group">
			<input type="text" name="feedback" class="form-control " placeholder="{{ trans("booking.feedback") }}" value="{{ request()->has("feedback") ? request()->get("feedback") : "" }}">
		</div>
		<div class="form-group">
			<input type="text" name="status" class="form-control " placeholder="{{ trans("booking.status") }}" value="{{ request()->has("status") ? request()->get("status") : "" }}">
		</div>

        <button class="btn btn-success" type="submit" ><i class="fa fa-search"></i></button>
        <a href="{{ url('admin/booking') }}" class="btn btn-danger" ><i class="fa fa-close"></i></a>
    </form>
@endpush

@section('content')
    @include(layoutTable() , ['title' => trans('booking.booking') , 'model' => 'booking' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection