		<tr>
			<th>
			{{ trans( "salons.salons") }}
			</th>
			<td>
				@php $salons = App\Application\Model\Salons::find($item->salons_id);  @endphp
				{{ is_json($salons->name) ? getDefaultValueKey($salons->name) :  $salons->name}}
			</td>
		</tr>
