@extends(layoutExtend('website'))
  @section('title')
    {{ trans('staff.staff') }} {{ trans('home.view') }}
@endsection
  @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('staff') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("staff.name") }}</th>
     <td>{{ nl2br($item->name) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("staff.details") }}</th>
     <td>{{ nl2br($item->details) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("staff.image") }}</th>
     <td>
     <img src="{{ small($item->image) }}" width="100" />
     </td>
    </tr>
  </table>
          @include('website.staff.buttons.delete' , ['id' => $item->id])
        @include('website.staff.buttons.edit' , ['id' => $item->id])
</div>
@endsection
