<h2>{{ ucfirst(trans('admin.Latest'))}} {{ ucfirst('salons') }}</h2>
<hr>
@php $sidebarSalons = \App\Application\Model\Salons::orderBy("id", "DESC")->limit(5)->get(); @endphp
		@if (count($sidebarSalons) > 0)
			@foreach ($sidebarSalons as $d)
				 <div>
					<p><a href="{{ url("salons/".$d->id."/view") }}">{{ str_limit($d->name , 20) }}</a></p > 
					<p><a href="{{ url("salons/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			