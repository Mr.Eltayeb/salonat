<h2>{{ ucfirst(trans('admin.Latest'))}} {{ ucfirst('shoprequest') }}</h2>
<hr>
@php $sidebarShoprequest = \App\Application\Model\Shoprequest::orderBy("id", "DESC")->limit(5)->get(); @endphp
		@if (count($sidebarShoprequest) > 0)
			@foreach ($sidebarShoprequest as $d)
				 <div>
					<p><a href="{{ url("shoprequest/".$d->id."/view") }}">{{ str_limit($d->item_quantity , 20) }}</a></p > 
					<p><a href="{{ url("shoprequest/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			