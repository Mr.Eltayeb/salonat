<h2>{{ ucfirst(trans('admin.Latest'))}} {{ ucfirst('shopitem') }}</h2>
<hr>
@php $sidebarShopitem = \App\Application\Model\Shopitem::orderBy("id", "DESC")->limit(5)->get(); @endphp
		@if (count($sidebarShopitem) > 0)
			@foreach ($sidebarShopitem as $d)
				 <div>
					<p><a href="{{ url("shopitem/".$d->id."/view") }}">{{ str_limit($d->name , 20) }}</a></p > 
					<p><a href="{{ url("shopitem/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			