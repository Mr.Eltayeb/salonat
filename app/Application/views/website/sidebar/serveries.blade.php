<h2>{{ ucfirst(trans('admin.Latest'))}} {{ ucfirst('serveries') }}</h2>
<hr>
@php $sidebarServeries = \App\Application\Model\Serveries::orderBy("id", "DESC")->limit(5)->get(); @endphp
		@if (count($sidebarServeries) > 0)
			@foreach ($sidebarServeries as $d)
				 <div>
					<p><a href="{{ url("serveries/".$d->id."/view") }}">{{ str_limit($d->name , 20) }}</a></p > 
					<p><a href="{{ url("serveries/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			