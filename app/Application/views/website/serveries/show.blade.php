@extends(layoutExtend('website'))
  @section('title')
    {{ trans('serveries.serveries') }} {{ trans('home.view') }}
@endsection
  @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('serveries') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("serveries.name") }}</th>
     <td>{{ nl2br($item->name) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("serveries.type") }}</th>
     <td>{{ nl2br($item->type) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("serveries.price") }}</th>
     <td>{{ nl2br($item->price) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("serveries.time") }}</th>
     <td>{{ nl2br($item->time) }}</td>
    </tr>
  </table>
          @include('website.serveries.buttons.delete' , ['id' => $item->id])
        @include('website.serveries.buttons.edit' , ['id' => $item->id])
</div>
@endsection
