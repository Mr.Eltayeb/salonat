		<div class="form-group {{ $errors->has("salons") ? "has-error" : "" }}">
			<label for="salons">{{ trans( "salons.salons") }}</label>
			@php $salons = App\Application\Model\Salons::pluck("name" ,"id")->all()  @endphp
			@php  $salons_id = isset($item) ? $item->salons_id : null @endphp
			<select name="salons_id"  class="form-control" >
			@foreach( $salons as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $salons_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
			@endforeach
			</select>
			@if ($errors->has("salons"))
				<div class="alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first("salons") }}</strong>
					</span>
				</div>
			@endif
			</div>
