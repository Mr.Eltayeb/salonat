@extends(layoutExtend('website'))

@section('title')
     {{ trans('shoprequest.shoprequest') }} {{ trans('home.control') }}
@endsection

@section('content')
 <div class="pull-{{ getDirection() }} col-lg-9">
    <div><h1>{{ trans('website.shoprequest') }}</h1></div>
     <div><a href="{{ url('shoprequest/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.shoprequest') }}</a><br></div>
 	<form method="get" class="form-inline">
		<div class="form-group">
			<input type="text" name="from" class="form-control datepicker2" placeholder="{{ trans("admin.from") }}"value="{{ request()->has("from") ? request()->get("from") : "" }}">
		 </div>
		<div class="form-group">
			<input type="text" name="to" class="form-control datepicker2" placeholder="{{ trans("admin.to") }}"value="{{ request()->has("to") ? request()->get("to") : "" }}">
		</div>
		<div class="form-group"> 
			<input type="text" name="item_quantity" class="form-control " placeholder="{{ trans("shoprequest.item_quantity") }}" value="{{ request()->has("item_quantity") ? request()->get("item_quantity") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="item_totel_price" class="form-control " placeholder="{{ trans("shoprequest.item_totel_price") }}" value="{{ request()->has("item_totel_price") ? request()->get("item_totel_price") : "" }}"> 
		</div> 
		 <button class="btn btn-success" type="submit" ><i class="fa fa-search" ></i ></button>
		<a href="{{ url("shoprequest") }}" class="btn btn-danger" ><i class="fa fa-close" ></i></a>
	 </form > 
<br ><table class="table table-responsive table-striped table-bordered"> 
		<thead > 
			<tr> 
				<th>{{ trans("shoprequest.item_quantity") }}</th> 
				<th>{{ trans("shoprequest.edit") }}</th> 
				<th>{{ trans("shoprequest.show") }}</th> 
				<th>{{
            trans("shoprequest.delete") }}</th> 
				</thead > 
		<tbody > 
		@if (count($items) > 0) 
			@foreach ($items as $d) 
				 <tr>
					<td>{{ str_limit($d->item_quantity , 20) }}</td> 
				<td> @include("website.shoprequest.buttons.edit", ["id" => $d->id])</td> 
					<td> @include("website.shoprequest.buttons.view", ["id" => $d->id])</td> 
					<td> @include("website.shoprequest.buttons.delete", ["id" => $d->id])</td> 
					</tr> 
					@endforeach
				@endif
			 </tbody > 
		</table > 
	@include(layoutPaginate() , ["items" => $items])
		
</div>
@endsection
