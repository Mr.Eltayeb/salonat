		<div class="form-group {{ $errors->has("shopitem") ? "has-error" : "" }}">
			<label for="shopitem">{{ trans( "shopitem.shopitem") }}</label>
			@php $shopitems = App\Application\Model\Shopitem::pluck("name" ,"id")->all()  @endphp
			@php  $shopitem_id = isset($item) ? $item->shopitem_id : null @endphp
			<select name="shopitem_id"  class="form-control" >
			@foreach( $shopitems as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $shopitem_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
			@endforeach
			</select>
			@if ($errors->has("shopitem"))
				<div class="alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first("shopitem") }}</strong>
					</span>
				</div>
			@endif
			</div>
