@extends(layoutExtend('website'))
 @section('title')
    {{ trans('shoprequest.shoprequest') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
 @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
         @include(layoutMessage('website'))
         <a href="{{ url('shoprequest') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('shoprequest/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("website.shoprequest.relation.user.edit")
            @include("website.shoprequest.relation.shopitem.edit")
                <div class="form-group {{ $errors->has("item_quantity") ? "has-error" : "" }}" > 
   <label for="item_quantity">{{ trans("shoprequest.item_quantity")}}</label>
    <input type="text" name="item_quantity" class="form-control" id="item_quantity" value="{{ isset($item->item_quantity) ? $item->item_quantity : old("item_quantity") }}"  placeholder="{{ trans("shoprequest.item_quantity")}}">
  </div>
   @if ($errors->has("item_quantity"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("item_quantity") }}</strong>
     </span>
    </div>
   @endif
   <div class="form-group {{ $errors->has("item_totel_price") ? "has-error" : "" }}" > 
   <label for="item_totel_price">{{ trans("shoprequest.item_totel_price")}}</label>
    <input type="text" name="item_totel_price" class="form-control" id="item_totel_price" value="{{ isset($item->item_totel_price) ? $item->item_totel_price : old("item_totel_price") }}"  placeholder="{{ trans("shoprequest.item_totel_price")}}">
  </div>
   @if ($errors->has("item_totel_price"))
    <div class="alert alert-danger">
     <span class='help-block'>
      <strong>{{ $errors->first("item_totel_price") }}</strong>
     </span>
    </div>
   @endif
             <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.shoprequest') }}
                </button>
            </div>
        </form>
</div>
@endsection
