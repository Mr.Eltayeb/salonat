@extends(layoutExtend('website'))
  @section('title')
    {{ trans('shoprequest.shoprequest') }} {{ trans('home.view') }}
@endsection
  @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('shoprequest') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("shoprequest.item_quantity") }}</th>
     <td>{{ nl2br($item->item_quantity) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("shoprequest.item_totel_price") }}</th>
     <td>{{ nl2br($item->item_totel_price) }}</td>
    </tr>
  </table>
          @include('website.shoprequest.buttons.delete' , ['id' => $item->id])
        @include('website.shoprequest.buttons.edit' , ['id' => $item->id])
</div>
@endsection
