@extends(layoutExtend('website'))

@section('title')
     {{ trans('shopitem.shopitem') }} {{ trans('home.control') }}
@endsection

@section('content')
 <div class="pull-{{ getDirection() }} col-lg-9">
    <div><h1>{{ trans('website.shopitem') }}</h1></div>
     <div><a href="{{ url('shopitem/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.shopitem') }}</a><br></div>
 	<form method="get" class="form-inline">
		<div class="form-group">
			<input type="text" name="from" class="form-control datepicker2" placeholder="{{ trans("admin.from") }}"value="{{ request()->has("from") ? request()->get("from") : "" }}">
		 </div>
		<div class="form-group">
			<input type="text" name="to" class="form-control datepicker2" placeholder="{{ trans("admin.to") }}"value="{{ request()->has("to") ? request()->get("to") : "" }}">
		</div>
		<div class="form-group"> 
			<input type="text" name="name" class="form-control " placeholder="{{ trans("shopitem.name") }}" value="{{ request()->has("name") ? request()->get("name") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="price" class="form-control " placeholder="{{ trans("shopitem.price") }}" value="{{ request()->has("price") ? request()->get("price") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="quantity" class="form-control " placeholder="{{ trans("shopitem.quantity") }}" value="{{ request()->has("quantity") ? request()->get("quantity") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="description" class="form-control " placeholder="{{ trans("shopitem.description") }}" value="{{ request()->has("description") ? request()->get("description") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="image_two" class="form-control " placeholder="{{ trans("shopitem.image_two") }}" value="{{ request()->has("image_two") ? request()->get("image_two") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="image_three" class="form-control " placeholder="{{ trans("shopitem.image_three") }}" value="{{ request()->has("image_three") ? request()->get("image_three") : "" }}"> 
		</div> 
		 <button class="btn btn-success" type="submit" ><i class="fa fa-search" ></i ></button>
		<a href="{{ url("shopitem") }}" class="btn btn-danger" ><i class="fa fa-close" ></i></a>
	 </form > 
<br ><table class="table table-responsive table-striped table-bordered"> 
		<thead > 
			<tr> 
				<th>{{ trans("shopitem.name") }}</th> 
				<th>{{ trans("shopitem.edit") }}</th> 
				<th>{{ trans("shopitem.show") }}</th> 
				<th>{{
            trans("shopitem.delete") }}</th> 
				</thead > 
		<tbody > 
		@if (count($items) > 0) 
			@foreach ($items as $d) 
				 <tr>
					<td>{{ str_limit($d->name , 20) }}</td> 
				<td> @include("website.shopitem.buttons.edit", ["id" => $d->id])</td> 
					<td> @include("website.shopitem.buttons.view", ["id" => $d->id])</td> 
					<td> @include("website.shopitem.buttons.delete", ["id" => $d->id])</td> 
					</tr> 
					@endforeach
				@endif
			 </tbody > 
		</table > 
	@include(layoutPaginate() , ["items" => $items])
		
</div>
@endsection
