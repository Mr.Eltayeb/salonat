@extends(layoutExtend('website'))

@section('title')
    {{ trans('shopitem.shopitem') }} {{ trans('home.view') }}
@endsection

@section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('shopitem') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
		 <table class="table table-bordered  table-striped" > 
				<tr>
				<th width="200">{{ trans("shopitem.name") }}</th>
					<td>{{ nl2br($item->name) }}</td>
				</tr>
				<tr>
				<th width="200">{{ trans("shopitem.price") }}</th>
					<td>{{ nl2br($item->price) }}</td>
				</tr>
				<tr>
				<th width="200">{{ trans("shopitem.quantity") }}</th>
					<td>{{ nl2br($item->quantity) }}</td>
				</tr>
				<tr>
				<th width="200">{{ trans("shopitem.description") }}</th>
					<td>{{ nl2br($item->description) }}</td>
				</tr>
				<tr>
				<th width="200">{{ trans("shopitem.image") }}</th>
					<td>
					<img src="{{ small($item->image) }}" width="100" />
					</td>
				</tr>
				<tr>
				<th width="200">{{ trans("shopitem.image_two") }}</th>
					<td>{{ nl2br($item->image_two) }}</td>
				</tr>
				<tr>
				<th width="200">{{ trans("shopitem.image_three") }}</th>
					<td>{{ nl2br($item->image_three) }}</td>
				</tr>
		</table>

        @include('website.shopitem.buttons.delete' , ['id' => $item->id])
        @include('website.shopitem.buttons.edit' , ['id' => $item->id])
</div>
@endsection
