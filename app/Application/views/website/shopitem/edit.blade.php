@extends(layoutExtend('website'))

@section('title')
    {{ trans('shopitem.shopitem') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
         @include(layoutMessage('website'))
         <a href="{{ url('shopitem') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('shopitem/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
             		 <div class="form-group {{ $errors->has("name") ? "has-error" : "" }}" > 
			<label for="name">{{ trans("shopitem.name")}}</label>
				<input type="text" name="name" class="form-control" id="name" value="{{ isset($item->name) ? $item->name : old("name") }}"  placeholder="{{ trans("shopitem.name")}}">
		</div>
			@if ($errors->has("name"))
				<div class="alert alert-danger">
					<span class='help-block'>
						<strong>{{ $errors->first("name") }}</strong>
					</span>
				</div>
			@endif
		 <div class="form-group {{ $errors->has("price") ? "has-error" : "" }}" > 
			<label for="price">{{ trans("shopitem.price")}}</label>
				<input type="text" name="price" class="form-control" id="price" value="{{ isset($item->price) ? $item->price : old("price") }}"  placeholder="{{ trans("shopitem.price")}}">
		</div>
			@if ($errors->has("price"))
				<div class="alert alert-danger">
					<span class='help-block'>
						<strong>{{ $errors->first("price") }}</strong>
					</span>
				</div>
			@endif
		 <div class="form-group {{ $errors->has("quantity") ? "has-error" : "" }}" > 
			<label for="quantity">{{ trans("shopitem.quantity")}}</label>
				<input type="text" name="quantity" class="form-control" id="quantity" value="{{ isset($item->quantity) ? $item->quantity : old("quantity") }}"  placeholder="{{ trans("shopitem.quantity")}}">
		</div>
			@if ($errors->has("quantity"))
				<div class="alert alert-danger">
					<span class='help-block'>
						<strong>{{ $errors->first("quantity") }}</strong>
					</span>
				</div>
			@endif
		 <div class="form-group {{ $errors->has("description") ? "has-error" : "" }}" > 
			<label for="description">{{ trans("shopitem.description")}}</label>
				<textarea name="description" class="form-control" id="description"   placeholder="{{ trans("shopitem.description")}}" >{{isset($item->description) ? $item->description : old("description") }}</textarea >
		</div>
			@if ($errors->has("description"))
				<div class="alert alert-danger">
					<span class='help-block'>
						<strong>{{ $errors->first("description") }}</strong>
					</span>
				</div>
			@endif
		 <div class="form-group {{ $errors->has("image") ? "has-error" : "" }}" > 
			<label for="image">{{ trans("shopitem.image")}}</label>
				@if(isset($item) && $item->image != "")
				<br>
				<img src="{{ small($item->image) }}" class="thumbnail" alt="" width="200">
				<br>
				@endif
				<input type="file" name="image" >
		</div>
			@if ($errors->has("image"))
				<div class="alert alert-danger">
					<span class='help-block'>
						<strong>{{ $errors->first("image") }}</strong>
					</span>
				</div>
			@endif
		 <div class="form-group {{ $errors->has("image_two") ? "has-error" : "" }}" > 
			<label for="image_two">{{ trans("shopitem.image_two")}}</label>
				<input type="text" name="image_two" class="form-control" id="image_two" value="{{ isset($item->image_two) ? $item->image_two : old("image_two") }}"  placeholder="{{ trans("shopitem.image_two")}}">
		</div>
			@if ($errors->has("image_two"))
				<div class="alert alert-danger">
					<span class='help-block'>
						<strong>{{ $errors->first("image_two") }}</strong>
					</span>
				</div>
			@endif
		 <div class="form-group {{ $errors->has("image_three") ? "has-error" : "" }}" > 
			<label for="image_three">{{ trans("shopitem.image_three")}}</label>
				<input type="text" name="image_three" class="form-control" id="image_three" value="{{ isset($item->image_three) ? $item->image_three : old("image_three") }}"  placeholder="{{ trans("shopitem.image_three")}}">
		</div>
			@if ($errors->has("image_three"))
				<div class="alert alert-danger">
					<span class='help-block'>
						<strong>{{ $errors->first("image_three") }}</strong>
					</span>
				</div>
			@endif

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.shopitem') }}
                </button>
            </div>
        </form>
</div>
@endsection
