<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('shoprequest') }}</h2>
<hr>
@php $sidebarShoprequest = \App\Application\Model\Shoprequest::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarShoprequest) > 0)
			@foreach ($sidebarShoprequest as $d)
				 <div>
					<h2 > {{ str_limit($d->item_quantity , 50) }}</h2 > 
					<p> {{ str_limit($d->item_totel_price , 300) }}</p > 
					 <p><a href="{{ url("shoprequest/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			