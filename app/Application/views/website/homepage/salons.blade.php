<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('salons') }}</h2>
<hr>
@php $sidebarSalons = \App\Application\Model\Salons::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarSalons) > 0)
			@foreach ($sidebarSalons as $d)
				 <div>
					<h2 > {{ str_limit($d->name , 50) }}</h2 > 
					<p> {{ str_limit($d->details , 300) }}</p > 
					 <img src="{{ small($d->image)}}"  width = "80" />
					 <p><a href="{{ url("salons/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			