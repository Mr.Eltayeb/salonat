<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('testerd') }}</h2>
<hr>
@php $sidebarTesterd = \App\Application\Model\Testerd::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarTesterd) > 0)
			@foreach ($sidebarTesterd as $d)
				 <div>
					<h2 > {{ str_limit($d->title , 50) }}</h2 > 
					 <img src="{{ small(getImageFromJson($d ,"image"))}}"  width = "80" />
					 <p><a href="{{ url("testerd/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			