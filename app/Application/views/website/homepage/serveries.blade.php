<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('serveries') }}</h2>
<hr>
@php $sidebarServeries = \App\Application\Model\Serveries::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarServeries) > 0)
			@foreach ($sidebarServeries as $d)
				 <div>
					<h2 > {{ str_limit($d->name , 50) }}</h2 > 
					<p> {{ str_limit($d->type , 300) }}</p > 
					<p> {{ str_limit($d->price , 300) }}</p > 
					 <p><a href="{{ url("serveries/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			