<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('staff') }}</h2>
<hr>
@php $sidebarStaff = \App\Application\Model\Staff::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarStaff) > 0)
			@foreach ($sidebarStaff as $d)
				 <div>
					<h2 > {{ str_limit($d->name , 50) }}</h2 > 
					<p> {{ str_limit($d->details , 300) }}</p > 
					 <img src="{{ small($d->image)}}"  width = "80" />
					 <p><a href="{{ url("staff/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			