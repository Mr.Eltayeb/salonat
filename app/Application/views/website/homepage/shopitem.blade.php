<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('shopitem') }}</h2>
<hr>
@php $sidebarShopitem = \App\Application\Model\Shopitem::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarShopitem) > 0)
			@foreach ($sidebarShopitem as $d)
				 <div>
					<h2 > {{ str_limit($d->name , 50) }}</h2 > 
					<p> {{ str_limit($d->price , 300) }}</p > 
					<p> {{ str_limit($d->quantity , 300) }}</p > 
					 <p><a href="{{ url("shopitem/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			