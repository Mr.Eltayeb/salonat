<h2>{{ ucfirst(trans('admin.Random'))}} {{ ucfirst('booking') }}</h2>
<hr>
@php $sidebarBooking = \App\Application\Model\Booking::inRandomOrder()->limit(5)->get(); @endphp
		@if (count($sidebarBooking) > 0)
			@foreach ($sidebarBooking as $d)
				 <div>
					<h2 > {{ str_limit($d->time , 50) }}</h2 > 
					<p> {{ str_limit($d->date , 300) }}</p > 
					<p> {{ str_limit($d->feedback , 300) }}</p > 
					 <p><a href="{{ url("booking/".$d->id."/view") }}" ><i class="fa fa-eye" ></i ></a> <small ><i class="fa fa-calendar-o" ></i > {{ $d->created_at }}</small ></p > 
				<hr > 
				</div> 
			@endforeach
		@endif
			