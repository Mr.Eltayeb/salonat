@extends(layoutExtend('website'))
  @section('title')
    {{ trans('salons.salons') }} {{ trans('home.view') }}
@endsection
  @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('salons') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("salons.name") }}</th>
     <td>{{ nl2br($item->name) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("salons.details") }}</th>
     <td>{{ nl2br($item->details) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("salons.image") }}</th>
     <td>
     <img src="{{ small($item->image) }}" width="100" />
     </td>
    </tr>
    <tr>
    <th width="200">{{ trans("salons.lat") }}</th>
     <td>
    {{nl2br($item->lat) }}
     </td></tr><tr><th>{{ trans("admin.location") }}</th>
     <td>
    <div id="showMap" style="width:100%;height: 500px;" data-lat="{{ $item->lat }}"  data-lng="{{ $item->lng }}"></div>
     </td>
    </tr>
    <tr>
    <th width="200">{{ trans("salons.lng") }}</th>
     <td>{{ nl2br($item->lng) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("salons.Address") }}</th>
     <td>{{ nl2br($item->Address) }}</td>
    </tr>
  </table>
          @include('website.salons.buttons.delete' , ['id' => $item->id])
        @include('website.salons.buttons.edit' , ['id' => $item->id])
</div>
@endsection
