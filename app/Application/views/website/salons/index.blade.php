@extends(layoutExtend('website'))

@section('title')
     {{ trans('salons.salons') }} {{ trans('home.control') }}
@endsection

@section('content')
 <div class="pull-{{ getDirection() }} col-lg-9">
    <div><h1>{{ trans('website.salons') }}</h1></div>
     <div><a href="{{ url('salons/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.salons') }}</a><br></div>
 	<form method="get" class="form-inline">
		<div class="form-group">
			<input type="text" name="from" class="form-control datepicker2" placeholder="{{ trans("admin.from") }}"value="{{ request()->has("from") ? request()->get("from") : "" }}">
		 </div>
		<div class="form-group">
			<input type="text" name="to" class="form-control datepicker2" placeholder="{{ trans("admin.to") }}"value="{{ request()->has("to") ? request()->get("to") : "" }}">
		</div>
		<div class="form-group"> 
			<input type="text" name="name" class="form-control " placeholder="{{ trans("salons.name") }}" value="{{ request()->has("name") ? request()->get("name") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="details" class="form-control " placeholder="{{ trans("salons.details") }}" value="{{ request()->has("details") ? request()->get("details") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="Address" class="form-control " placeholder="{{ trans("salons.Address") }}" value="{{ request()->has("Address") ? request()->get("Address") : "" }}"> 
		</div> 
		 <button class="btn btn-success" type="submit" ><i class="fa fa-search" ></i ></button>
		<a href="{{ url("salons") }}" class="btn btn-danger" ><i class="fa fa-close" ></i></a>
	 </form > 
<br ><table class="table table-responsive table-striped table-bordered"> 
		<thead > 
			<tr> 
				<th>{{ trans("salons.name") }}</th> 
				<th>{{ trans("salons.edit") }}</th> 
				<th>{{ trans("salons.show") }}</th> 
				<th>{{
            trans("salons.delete") }}</th> 
				</thead > 
		<tbody > 
		@if (count($items) > 0) 
			@foreach ($items as $d) 
				 <tr>
					<td>{{ str_limit($d->name , 20) }}</td> 
				<td> @include("website.salons.buttons.edit", ["id" => $d->id])</td> 
					<td> @include("website.salons.buttons.view", ["id" => $d->id])</td> 
					<td> @include("website.salons.buttons.delete", ["id" => $d->id])</td> 
					</tr> 
					@endforeach
				@endif
			 </tbody > 
		</table > 
	@include(layoutPaginate() , ["items" => $items])
		
</div>
@endsection
