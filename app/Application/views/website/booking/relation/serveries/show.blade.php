		<tr>
			<th>
			{{ trans( "serveries.serveries") }}
			</th>
			<td>
				@php $serveries = App\Application\Model\Serveries::find($item->serveries_id);  @endphp
				{{ is_json($serveries->name) ? getDefaultValueKey($serveries->name) :  $serveries->name}}
			</td>
		</tr>
