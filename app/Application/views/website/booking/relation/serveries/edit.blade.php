		<div class="form-group {{ $errors->has("serveries") ? "has-error" : "" }}">
			<label for="serveries">{{ trans( "serveries.serveries") }}</label>
			@php $serveries = App\Application\Model\Serveries::pluck("name" ,"id")->all()  @endphp
			@php  $serveries_id = isset($item) ? $item->serveries_id : null @endphp
			<select name="serveries_id"  class="form-control" >
			@foreach( $serveries as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $serveries_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</option>
			@endforeach
			</select>
			@if ($errors->has("serveries"))
				<div class="alert alert-danger">
					<span class="help-block">
						<strong>{{ $errors->first("serveries") }}</strong>
					</span>
				</div>
			@endif
			</div>
