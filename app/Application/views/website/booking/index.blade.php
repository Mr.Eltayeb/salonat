@extends(layoutExtend('website'))

@section('title')
     {{ trans('booking.booking') }} {{ trans('home.control') }}
@endsection

@section('content')
 <div class="pull-{{ getDirection() }} col-lg-9">
    <div><h1>{{ trans('website.booking') }}</h1></div>
     <div><a href="{{ url('booking/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.booking') }}</a><br></div>
 	<form method="get" class="form-inline">
		<div class="form-group">
			<input type="text" name="from" class="form-control datepicker2" placeholder="{{ trans("admin.from") }}"value="{{ request()->has("from") ? request()->get("from") : "" }}">
		 </div>
		<div class="form-group">
			<input type="text" name="to" class="form-control datepicker2" placeholder="{{ trans("admin.to") }}"value="{{ request()->has("to") ? request()->get("to") : "" }}">
		</div>
		<div class="form-group"> 
			<input type="text" name="time" class="form-control time" placeholder="{{ trans("booking.time") }}" value="{{ request()->has("time") ? request()->get("time") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="date" class="form-control datepicker2" placeholder="{{ trans("booking.date") }}" value="{{ request()->has("date") ? request()->get("date") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="feedback" class="form-control " placeholder="{{ trans("booking.feedback") }}" value="{{ request()->has("feedback") ? request()->get("feedback") : "" }}"> 
		</div> 
		<div class="form-group"> 
			<input type="text" name="status" class="form-control " placeholder="{{ trans("booking.status") }}" value="{{ request()->has("status") ? request()->get("status") : "" }}"> 
		</div> 
		 <button class="btn btn-success" type="submit" ><i class="fa fa-search" ></i ></button>
		<a href="{{ url("booking") }}" class="btn btn-danger" ><i class="fa fa-close" ></i></a>
	 </form > 
<br ><table class="table table-responsive table-striped table-bordered"> 
		<thead > 
			<tr> 
				<th>{{ trans("booking.time") }}</th> 
				<th>{{ trans("booking.edit") }}</th> 
				<th>{{ trans("booking.show") }}</th> 
				<th>{{
            trans("booking.delete") }}</th> 
				</thead > 
		<tbody > 
		@if (count($items) > 0) 
			@foreach ($items as $d) 
				 <tr>
					<td>{{ str_limit($d->time , 20) }}</td> 
				<td> @include("website.booking.buttons.edit", ["id" => $d->id])</td> 
					<td> @include("website.booking.buttons.view", ["id" => $d->id])</td> 
					<td> @include("website.booking.buttons.delete", ["id" => $d->id])</td> 
					</tr> 
					@endforeach
				@endif
			 </tbody > 
		</table > 
	@include(layoutPaginate() , ["items" => $items])
		
</div>
@endsection
