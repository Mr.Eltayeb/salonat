@extends(layoutExtend('website'))
  @section('title')
    {{ trans('booking.booking') }} {{ trans('home.view') }}
@endsection
  @section('content')
<div class="pull-{{ getDirection() }} col-lg-9">
        <a href="{{ url('booking') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
   <table class="table table-bordered  table-striped" > 
    <tr>
    <th width="200">{{ trans("booking.time") }}</th>
     <td>{{ nl2br($item->time) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("booking.date") }}</th>
     <td>{{ nl2br($item->date) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("booking.feedback") }}</th>
     <td>{{ nl2br($item->feedback) }}</td>
    </tr>
    <tr>
    <th width="200">{{ trans("booking.status") }}</th>
     <td>{{ nl2br($item->status) }}</td>
    </tr>
  </table>
          @include('website.booking.buttons.delete' , ['id' => $item->id])
        @include('website.booking.buttons.edit' , ['id' => $item->id])
</div>
@endsection
