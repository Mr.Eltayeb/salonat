<?php
 namespace App\Application\Requests\Admin\Shoprequest;
 use Illuminate\Foundation\Http\FormRequest;
 class AddRequestShoprequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	"user_id" => "required|integer",
         "shopitem_id" => "required|integer",
            "item_quantity" => "",
   "item_totel_price" => "",
            ];
    }
}
