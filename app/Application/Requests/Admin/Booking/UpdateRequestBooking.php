<?php
 namespace App\Application\Requests\Admin\Booking;
 use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
 class UpdateRequestBooking extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Route::input('id');
        return [
        	"staff_id" => "required|integer",
         "serveries_id" => "required|integer",
         "user_id" => "required|integer",
         "salons_id" => "required|integer",
            "time" => "required",
   "date" => "required|date",
   "feedback" => "min:5|max:121|nullable",
   "status" => "required",
            ];
    }
}
