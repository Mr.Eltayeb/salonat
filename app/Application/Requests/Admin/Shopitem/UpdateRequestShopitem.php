<?php

namespace App\Application\Requests\Admin\Shopitem;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class UpdateRequestShopitem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Route::input('id');
        return [
            "name" => "min:4|max:50|required",
			"price" => "required",
			"quantity" => "required",
			"description" => "",
			"image" => "image",
			"image_two" => "image",
			"image_three" => "image",
			
        ];
    }
}
