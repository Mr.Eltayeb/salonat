<?php
 namespace App\Application\Requests\Admin\Serveries;
 use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
 class UpdateRequestServeries extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Route::input('id');
        return [
        	"salons_id" => "required|integer",
            "name" => "",
   "type" => "min:3|max:161",
   "price" => "",
   "time" => "",
            ];
    }
}
