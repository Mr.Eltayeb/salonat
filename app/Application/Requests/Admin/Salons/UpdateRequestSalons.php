<?php

namespace App\Application\Requests\Admin\Salons;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class UpdateRequestSalons extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Route::input('id');
        return [
            "user_id" => "required|integer",
            "name" => "min:3|max:161|required",
            "details" => "min:3|max:161|required",
            "image" => "",
            "lat" => "nullable",
            "lng" => "nullable",
            "Address" => "min:5|max:161|required",
            "photo" => "",
            "photo_two" => "",
            "photo_three" => "",
            "logo" => ""
        ];
    }
}
