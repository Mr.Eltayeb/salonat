<?php
 namespace App\Application\Requests\Website\Shoprequest;
 use Illuminate\Support\Facades\Route;
 class ApiUpdateRequestShoprequest
{
    public function rules()
    {
        $id = Route::input('id');
        return [
        	"user_id" => "required|integer",
         "shopitem_id" => "required|integer",
            "item_quantity" => "",
   "item_totel_price" => "",
            ];
    }
}
