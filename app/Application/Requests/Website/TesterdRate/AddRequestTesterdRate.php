<?php

namespace App\Application\Requests\Website\TesterdRate;

use Illuminate\Foundation\Http\FormRequest;

class AddRequestTesterdRate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user_id" => "",
			"testerd_id" => "",
			"rate" => "min:1|max:5|required|integer",
			
        ];
    }
}
