<?php

namespace App\Application\Requests\Website\Shopitem;

use Illuminate\Foundation\Http\FormRequest;

class AddRequestShopitem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "min:4|max:50|required",
			"price" => "required",
			"quantity" => "required",
			"description" => "",
			"image" => "image",
			"image_two" => "image",
			"image_three" => "image",
			
        ];
    }
}
