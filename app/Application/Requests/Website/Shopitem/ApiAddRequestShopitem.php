<?php

namespace App\Application\Requests\Website\Shopitem;


class ApiAddRequestShopitem
{
    public function rules()
    {
        return [
            "name" => "min:4|max:50|requiredprice",
			"image_three" => "image",
			
        ];
    }
}
