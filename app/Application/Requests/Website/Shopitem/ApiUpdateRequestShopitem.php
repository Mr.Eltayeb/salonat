<?php

namespace App\Application\Requests\Website\Shopitem;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestShopitem
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "name" => "min:4|max:50|requiredprice",
			"image_three" => "image",
			
        ];
    }
}
