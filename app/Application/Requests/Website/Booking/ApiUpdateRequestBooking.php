<?php

namespace App\Application\Requests\Website\Booking;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestBooking
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "staff_id" => "integer",
            "serveries_id" => "integer",
            "user_id" => "integer",
            "salons_id" => "integer",
            "time" => "date",
            "status" => "integer",
        ];
    }
}
