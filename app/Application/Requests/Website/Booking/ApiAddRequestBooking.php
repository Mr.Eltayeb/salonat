<?php
 namespace App\Application\Requests\Website\Booking;
class ApiAddRequestBooking
{
    public function rules()
    {
        return [
        	"staff_id" => "required|integer",
            "serveries_id" => "required|integer",
            "user_id" => "required|integer",
            "salons_id" => "required|integer",
            "time" => "required",
            "date" => "required|date",
            "feedback" => "min:5|max:121|nullable",
            "status" => "required",
        ];
    }
}
