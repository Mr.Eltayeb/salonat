<?php

namespace App\Application\Requests\Website\Booking;

use Illuminate\Support\Facades\Route;

class ApiUpdateStatus
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "status" => "integer",
        ];
    }
}
