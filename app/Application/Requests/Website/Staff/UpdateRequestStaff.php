<?php
 namespace App\Application\Requests\Website\Staff;
 use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
 class UpdateRequestStaff extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Route::input('id');
        return [
        	"user_id" => "required|integer",
         "salons_id" => "required|integer",
            "name" => "min:3|max:161|required",
   "details" => "min:3|max:161|nullable",
   "image" => "image",
            ];
    }
}
