<?php
 namespace App\Application\Requests\Website\Staff;
 use Illuminate\Support\Facades\Route;
 class ApiUpdateRequestStaff
{
    public function rules()
    {
        $id = Route::input('id');
        return [
        	"user_id" => "required|integer",
         "salons_id" => "required|integer",
            "name" => "min:3|max:161|requireddetails",
   "image" => "image",
            ];
    }
}
