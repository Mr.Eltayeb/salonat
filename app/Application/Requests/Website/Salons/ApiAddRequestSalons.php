<?php
 namespace App\Application\Requests\Website\Salons;
  class ApiAddRequestSalons
{
    public function rules()
    {
        return [
        	"user_id" => "required|integer",
            "name" => "min:3|max:161|requireddetails",
   "Address" => "min:5|max:161|required",
            ];
    }
}
