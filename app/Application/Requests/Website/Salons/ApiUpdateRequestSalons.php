<?php
 namespace App\Application\Requests\Website\Salons;
 use Illuminate\Support\Facades\Route;
 class ApiUpdateRequestSalons
{
    public function rules()
    {
        $id = Route::input('id');
        return [
        	"user_id" => "required|integer",
            "name" => "min:3|max:161|requireddetails",
   "Address" => "min:5|max:161|required",
            ];
    }
}
