<?php

#### page control
Route::get('page/item/{id?}', 'PageController@show');
Route::post('page/item', 'PageController@store');
Route::post('page/item/{id}', 'PageController@update');
Route::get('page/{id}/delete', 'PageController@destroy');
#### page comment
Route::post('page/add/comment/{id}', 'PageCommentController@addComment');
Route::post('page/update/comment/{id}', 'PageCommentController@updateComment');
Route::get('page/delete/comment/{id}', 'PageCommentController@deleteComment');


#### categorie control
Route::get('categorie', 'CategorieController@index');
Route::get('categorie/item/{id?}', 'CategorieController@show');
Route::post('categorie/item', 'CategorieController@store');
Route::post('categorie/item/{id}', 'CategorieController@update');
Route::get('categorie/{id}/delete', 'CategorieController@destroy');
Route::get('categorie/{id}/view', 'CategorieController@getById');



#### salons control
Route::get('salonsapi' , 'SalonsController@salons');
Route::get('salons' , 'SalonsController@index');
Route::get('salons/item/{id?}' , 'SalonsController@show');
Route::post('salons/item' , 'SalonsController@store');
Route::post('salons/item/{id}' , 'SalonsController@update');
Route::get('salons/{id}/delete' , 'SalonsController@destroy');
Route::get('salons/{id}/view' , 'SalonsController@getById');



#### staff control
Route::get('staff' , 'StaffController@index');
Route::get('staff/item/{id?}' , 'StaffController@show');
Route::post('staff/item' , 'StaffController@store');
Route::post('staff/item/{id}' , 'StaffController@update');
Route::get('staff/{id}/delete' , 'StaffController@destroy');
Route::get('staff/{id}/view' , 'StaffController@getById');


#### booking control
Route::get('booking' , 'BookingController@index');
Route::get('booking/item/{id?}' , 'BookingController@show');
Route::post('booking/item' , 'BookingController@store');
Route::post('booking/item/{id}' , 'BookingController@update');
Route::get('booking/{id}/delete' , 'BookingController@destroy');
Route::get('booking/{id}/view' , 'BookingController@getById');

#### serveries control
Route::get('serveries' , 'ServeriesController@index');
Route::get('serveries/item/{id?}' , 'ServeriesController@show');
Route::post('serveries/item' , 'ServeriesController@store');
Route::post('serveries/item/{id}' , 'ServeriesController@update');
Route::get('serveries/{id}/delete' , 'ServeriesController@destroy');
Route::get('serveries/{id}/view' , 'ServeriesController@getById');



#### testerd control
Route::get('testerd' , 'TesterdController@index');
Route::get('testerd/item/{id?}' , 'TesterdController@show');
Route::post('testerd/item' , 'TesterdController@store');
Route::post('testerd/item/{id}' , 'TesterdController@update');
Route::get('testerd/{id}/delete' , 'TesterdController@destroy');
Route::get('testerd/{id}/view' , 'TesterdController@getById');
#### testerd Rate
Route::post('testerd/add/rate/{id}' , 'TesterdRateController@addRate');




#### shopitem control
Route::get('shopitem' , 'ShopitemController@index');
Route::get('shopitem/item/{id?}' , 'ShopitemController@show');
Route::post('shopitem/item' , 'ShopitemController@store');
Route::post('shopitem/item/{id}' , 'ShopitemController@update');
Route::get('shopitem/{id}/delete' , 'ShopitemController@destroy');
Route::get('shopitem/{id}/view' , 'ShopitemController@getById');

#### shoprequest control
Route::get('shoprequest' , 'ShoprequestController@index');
Route::get('shoprequest/item/{id?}' , 'ShoprequestController@show');
Route::post('shoprequest/item' , 'ShoprequestController@store');
Route::post('shoprequest/item/{id}' , 'ShoprequestController@update');
Route::get('shoprequest/{id}/delete' , 'ShoprequestController@destroy');
Route::get('shoprequest/{id}/view' , 'ShoprequestController@getById');