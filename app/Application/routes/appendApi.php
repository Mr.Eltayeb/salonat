<?php

#user
Route::post('users/login', 'UserApi@login');
Route::get('users/getById/{id}', 'UserApi@getById');
Route::get('users/delete/{id}', 'UserApi@delete');
Route::post('users/add', 'UserApi@add');
Route::post('users/update', 'UserApi@update');
Route::get('users', 'UserApi@index');
Route::get('users/getUserByToken', 'UserApi@getUserByToken');

#page
Route::get('page/getById/{id}', 'PageApi@getById');
Route::get('page/delete/{id}', 'PageApi@delete');
Route::post('page/add', 'PageApi@add');
Route::post('page/update/{id}', 'PageApi@update');
Route::get('page', 'PageApi@index');

#categorie
Route::get('categorie/getById/{id}', 'CategorieApi@getById');
Route::get('categorie/delete/{id}', 'CategorieApi@delete');
Route::post('categorie/add', 'CategorieApi@add');
Route::post('categorie/update/{id}', 'CategorieApi@update');
Route::get('categorie', 'CategorieApi@index');


#salons
Route::get('getsalons/getAll', 'SalonsApi@salons');
Route::get('salons/getById/{id}', 'SalonsApi@getById');
Route::get('salons/delete/{id}', 'SalonsApi@delete');
Route::post('salons/add', 'SalonsApi@add');
Route::post('salons/update/{id}', 'SalonsApi@update');
Route::get('salons', 'SalonsApi@index');


#staff
Route::get('getstaff', 'StaffApi@staffs');
Route::get('staff/getById/{id}', 'StaffApi@getById');
Route::get('staff/delete/{id}', 'StaffApi@delete');
Route::post('staff/add', 'StaffApi@add');
Route::post('staff/update/{id}', 'StaffApi@update');
Route::get('staff', 'StaffApi@index');


#booking
Route::get('booking/getById/{id}', 'BookingApi@getById');
Route::get('booking/getByStaffId/{id}/{lang?}', 'BookingApi@getByStaffId');
Route::get('booking/getByUser/{id}/{lang?}', 'BookingApi@getByUser');
Route::get('booking/getByAdmin/{adminID}/{lang?}', 'BookingApi@getByAdmin');
Route::get('booking/delete/{id}', 'BookingApi@delete');
Route::post('booking/add', 'BookingApi@add');
Route::post('booking/update/{id}', 'BookingApi@update');
Route::post('booking/updateStatus/{id}', 'BookingApi@updateStatus');
Route::get('booking', 'BookingApi@index');

#serveries
Route::get('serveries/getById/{id}', 'ServeriesApi@getById');
Route::get('serveries/delete/{id}', 'ServeriesApi@delete');
Route::post('serveries/add', 'ServeriesApi@add');
Route::post('serveries/update/{id}', 'ServeriesApi@update');
Route::get('serveries', 'ServeriesApi@index');


#testerd
Route::get('testerd/getById/{id}', 'TesterdApi@getById');
Route::get('testerd/delete/{id}', 'TesterdApi@delete');
Route::post('testerd/add', 'TesterdApi@add');
Route::post('testerd/update/{id}', 'TesterdApi@update');
Route::get('testerd', 'TesterdApi@index');



#shopitem
Route::get('shopitem/getById/{id}', 'ShopitemApi@getById');
Route::get('shopitem/delete/{id}', 'ShopitemApi@delete');
Route::post('shopitem/add', 'ShopitemApi@add');
Route::post('shopitem/update/{id}', 'ShopitemApi@update');
Route::get('shopitem', 'ShopitemApi@index');

#shoprequest
Route::get('shoprequest/getById/{id}', 'ShoprequestApi@getById');
Route::get('shoprequest/delete/{id}', 'ShoprequestApi@delete');
Route::post('shoprequest/add', 'ShoprequestApi@add');
Route::post('shoprequest/update/{id}', 'ShoprequestApi@update');
Route::get('shoprequest', 'ShoprequestApi@index');