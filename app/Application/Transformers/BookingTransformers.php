<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class BookingTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {

        return [
            "id" => $modelOrCollection->id,
            "time" => $modelOrCollection->time,
            "date" => $modelOrCollection->date,
            "feedback" => $modelOrCollection->feedback,
            "salons_id" => $modelOrCollection->salons_id,
            "serveries_id" => $modelOrCollection->serveries_id,
            "staff_id" => $modelOrCollection->staff_id,
            "user_id" => $modelOrCollection->user_id,
            "status" => $modelOrCollection->status,
            "updated_at" => $modelOrCollection->updated_at,
            "salons" => $modelOrCollection->salons,
            "serveries" => $modelOrCollection->serveries,
            "staff" => $modelOrCollection->staff,
            // "user" => $modelOrCollection->user,

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {

        return [
            "id" => $modelOrCollection->id,
            "time" => $modelOrCollection->time,
            "date" => $modelOrCollection->date,
            "feedback" => $modelOrCollection->feedback,
            "salons_id" => $modelOrCollection->salons_id,
            "serveries_id" => $modelOrCollection->serveries_id,
            "staff_id" => $modelOrCollection->staff_id,
            "user_id" => $modelOrCollection->user_id,
            "status" => $modelOrCollection->status,
            "salons" => $modelOrCollection->salons,
            "serveries" => $modelOrCollection->serveries,
            "staff" => $modelOrCollection->staff,
            "user" => $modelOrCollection->user,

        ];
    }

}