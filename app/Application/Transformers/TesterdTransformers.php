<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class TesterdTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"title" => $modelOrCollection->title,
			"image" => $modelOrCollection->image,
			"testerdrate" => $modelOrCollection->testerdrate,

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"title" => $modelOrCollection->title,
			"image" => $modelOrCollection->image,
			"testerdrate" => $modelOrCollection->testerdrate,

        ];
    }

}