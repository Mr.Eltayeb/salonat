<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class ShopitemTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"name" => $modelOrCollection->name,
			"price" => $modelOrCollection->price,
			"quantity" => $modelOrCollection->quantity,
			"description" => $modelOrCollection->description,
			"image" => $modelOrCollection->image,
			"image_two" => $modelOrCollection->image_two,
			"image_three" => $modelOrCollection->image_three,

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"name" => $modelOrCollection->name,
			"price" => $modelOrCollection->price,
			"quantity" => $modelOrCollection->quantity,
			"description" => $modelOrCollection->description,
			"image" => $modelOrCollection->image,
			"image_two" => $modelOrCollection->image_two,
			"image_three" => $modelOrCollection->image_three,

        ];
    }

}