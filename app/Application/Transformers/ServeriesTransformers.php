<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class ServeriesTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    { return [
        "id" => $modelOrCollection->id,
        "name" => $modelOrCollection->name,
        "type" => $modelOrCollection->type,
        "price" => $modelOrCollection->price,
        "time" => $modelOrCollection->time,

    ];
    }

    public function transformModelAr(Model $modelOrCollection)
    { return [
        "id" => $modelOrCollection->id,
        "name" => $modelOrCollection->name,
        "type" => $modelOrCollection->type,
        "price" => $modelOrCollection->price,
        "time" => $modelOrCollection->time,

    ];
    }

}