<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class StaffTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
            "name" => $modelOrCollection->name,
            "details" => $modelOrCollection->details,
            "salons_id" => $modelOrCollection->salons_id,
            "image" => url(env('UPLOAD_PATH') . '/' . $modelOrCollection->image),


        ];

    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
            "name" => $modelOrCollection->name,
            "details" => $modelOrCollection->details,
            "salons_id" => $modelOrCollection->salons_id,
            "image" => url(env('UPLOAD_PATH') . '/' . $modelOrCollection->image),


        ];

    }

}