<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class SalonsTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
            "name" => $modelOrCollection->name,
            "details" => $modelOrCollection->details,
            "image" => url(env('UPLOAD_PATH'). '/'.$modelOrCollection->image),
            "lat" => $modelOrCollection->lat,
            "lng" => $modelOrCollection->lng,
            "Address" => $modelOrCollection->Address,
            "serveries" => $modelOrCollection->serveries,
            "staff" => $modelOrCollection->staff,
            "photo" =>  url(env('UPLOAD_PATH'). '/'.$modelOrCollection->photo),
            "photo_two" =>  url(env('UPLOAD_PATH'). '/'.$modelOrCollection->photo_two),
            "photo_three" =>  url(env('UPLOAD_PATH'). '/'.$modelOrCollection->photo_three),
            "logo" =>  url(env('UPLOAD_PATH'). '/'.$modelOrCollection->logo),
            
        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
            "name" => $modelOrCollection->name,
            "details" => $modelOrCollection->details,
            "image" => url(env('UPLOAD_PATH'). '/'.$modelOrCollection->image),
            "lat" => $modelOrCollection->lat,
            "lng" => $modelOrCollection->lng,
            "Address" => $modelOrCollection->Address,
            "serveries" => $modelOrCollection->serveries,
            "staff" => $modelOrCollection->staff,
//            "photo" => $modelOrCollection->photo,
            "photo" =>  url(env('UPLOAD_PATH'). '/'.$modelOrCollection->photo),
            "photo_two" =>  url(env('UPLOAD_PATH'). '/'.$modelOrCollection->photo_two),
            "photo_three" =>  url(env('UPLOAD_PATH'). '/'.$modelOrCollection->photo_three),
            "logo" =>  url(env('UPLOAD_PATH'). '/'.$modelOrCollection->logo),


        ];
    }

}