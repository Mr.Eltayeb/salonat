<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class ShoprequestTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"item_quantity" => $modelOrCollection->item_quantity,
			"item_totel_price" => $modelOrCollection->item_totel_price,

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"item_quantity" => $modelOrCollection->item_quantity,
			"item_totel_price" => $modelOrCollection->item_totel_price,

        ];
    }

}