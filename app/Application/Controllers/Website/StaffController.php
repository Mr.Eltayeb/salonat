<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Staff;
use App\Application\Requests\Website\Staff\AddRequestStaff;
use App\Application\Requests\Website\Staff\UpdateRequestStaff;

class StaffController extends AbstractController
{

     public function __construct(Staff $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("name") && request()->get("name") != ""){
				$items = $items->where("name","=", request()->get("name"));
			}

			if(request()->has("details") && request()->get("details") != ""){
				$items = $items->where("details","=", request()->get("details"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.staff.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.staff.edit' , $id);
     }

     public function store(AddRequestStaff $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('staff');
     }

     public function update($id , UpdateRequestStaff $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.staff.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'staff')->with('sucess' , 'Done Delete Staff From system');
     }


}
