<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Salons;
use App\Application\Requests\Website\Salons\AddRequestSalons;
use App\Application\Requests\Website\Salons\UpdateRequestSalons;

class SalonsController extends AbstractController
{

     public function __construct(Salons $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("name") && request()->get("name") != ""){
				$items = $items->where("name","=", request()->get("name"));
			}

			if(request()->has("details") && request()->get("details") != ""){
				$items = $items->where("details","=", request()->get("details"));
			}

			if(request()->has("Address") && request()->get("Address") != ""){
				$items = $items->where("Address","=", request()->get("Address"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.salons.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.salons.edit' , $id);
     }

     public function store(AddRequestSalons $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('salons');
     }

     public function update($id , UpdateRequestSalons $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.salons.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'salons')->with('sucess' , 'Done Delete Salons From system');
     }

     public function salons($lang = "en"){
        $data = Salons::all();
                if ($data) {
                    return response()->json(['data' =>$data], 200);
                }
                return response(apiReturn('null', '', 'No Data Found'), 200);    
            }

}
