<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Booking;
use App\Application\Requests\Website\Booking\AddRequestBooking;
use App\Application\Requests\Website\Booking\UpdateRequestBooking;

class BookingController extends AbstractController
{

     public function __construct(Booking $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("time") && request()->get("time") != ""){
				$items = $items->where("time","=", request()->get("time"));
			}

			if(request()->has("date") && request()->get("date") != ""){
				$items = $items->where("date","=", request()->get("date"));
			}

			if(request()->has("feedback") && request()->get("feedback") != ""){
				$items = $items->where("feedback","=", request()->get("feedback"));
			}

			if(request()->has("status") && request()->get("status") != ""){
				$items = $items->where("status","=", request()->get("status"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.booking.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.booking.edit' , $id);
     }

     public function store(AddRequestBooking $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('booking');
     }

     public function update($id , UpdateRequestBooking $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.booking.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'booking')->with('sucess' , 'Done Delete Booking From system');
     }


}
