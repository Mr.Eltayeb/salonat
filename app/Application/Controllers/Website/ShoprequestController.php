<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Shoprequest;
use App\Application\Requests\Website\Shoprequest\AddRequestShoprequest;
use App\Application\Requests\Website\Shoprequest\UpdateRequestShoprequest;

class ShoprequestController extends AbstractController
{

     public function __construct(Shoprequest $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("item_quantity") && request()->get("item_quantity") != ""){
				$items = $items->where("item_quantity","=", request()->get("item_quantity"));
			}

			if(request()->has("item_totel_price") && request()->get("item_totel_price") != ""){
				$items = $items->where("item_totel_price","=", request()->get("item_totel_price"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.shoprequest.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.shoprequest.edit' , $id);
     }

     public function store(AddRequestShoprequest $request){
//         $request
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('shoprequest');
     }

     public function update($id , UpdateRequestShoprequest $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.shoprequest.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'shoprequest')->with('sucess' , 'Done Delete Shoprequest From system');
     }


}
