<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Testerd;
use App\Application\Requests\Website\Testerd\AddRequestTesterd;
use App\Application\Requests\Website\Testerd\UpdateRequestTesterd;

class TesterdController extends AbstractController
{

     public function __construct(Testerd $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("title") && request()->get("title") != ""){
				$items = $items->where("title","=", request()->get("title"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.testerd.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.testerd.edit' , $id);
     }

     public function store(AddRequestTesterd $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('testerd');
     }

     public function update($id , UpdateRequestTesterd $request){
          if ($request->has("oldFiles_image") && $request->oldFiles_image != "") {
                                        $oldImage_image = $request->oldFiles_image;
                                        $request->request->remove("oldFiles_image");
                                    } else {
                                        $oldImage_image = json_encode([]);
                                    }
$item = $this->storeOrUpdate($request, $id, true);
if ($item) {
                                    $image = json_decode($item->image) ?? [];
                                    $newIamge = json_decode($oldImage_image) ?? [];
                                    $item_image = array_unique(array_merge($image, $newIamge));
                                    $item->image = json_encode($item_image);
                                    $item->save();
                                }
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.testerd.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'testerd')->with('sucess' , 'Done Delete Testerd From system');
     }


}
