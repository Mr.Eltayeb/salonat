<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Shopitem;
use App\Application\Requests\Website\Shopitem\AddRequestShopitem;
use App\Application\Requests\Website\Shopitem\UpdateRequestShopitem;

class ShopitemController extends AbstractController
{

     public function __construct(Shopitem $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("name") && request()->get("name") != ""){
				$items = $items->where("name","=", request()->get("name"));
			}

			if(request()->has("price") && request()->get("price") != ""){
				$items = $items->where("price","=", request()->get("price"));
			}

			if(request()->has("quantity") && request()->get("quantity") != ""){
				$items = $items->where("quantity","=", request()->get("quantity"));
			}

			if(request()->has("description") && request()->get("description") != ""){
				$items = $items->where("description","=", request()->get("description"));
			}

			if(request()->has("image_two") && request()->get("image_two") != ""){
				$items = $items->where("image_two","=", request()->get("image_two"));
			}

			if(request()->has("image_three") && request()->get("image_three") != ""){
				$items = $items->where("image_three","=", request()->get("image_three"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.shopitem.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.shopitem.edit' , $id);
     }

     public function store(AddRequestShopitem $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('shopitem');
     }

     public function update($id , UpdateRequestShopitem $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.shopitem.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'shopitem')->with('sucess' , 'Done Delete Shopitem From system');
     }


}
