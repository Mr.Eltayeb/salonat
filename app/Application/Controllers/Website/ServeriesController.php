<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Serveries;
use App\Application\Requests\Website\Serveries\AddRequestServeries;
use App\Application\Requests\Website\Serveries\UpdateRequestServeries;

class ServeriesController extends AbstractController
{

     public function __construct(Serveries $model)
     {
        parent::__construct($model);
     }

     public function index(){
        $items = $this->model;

        if(request()->has('from') && request()->get('from') != ''){
            $items = $items->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $items = $items->whereDate('created_at' , '<=' , request()->get('to'));
        }

			if(request()->has("name") && request()->get("name") != ""){
				$items = $items->where("name","=", request()->get("name"));
			}

			if(request()->has("type") && request()->get("type") != ""){
				$items = $items->where("type","=", request()->get("type"));
			}

			if(request()->has("price") && request()->get("price") != ""){
				$items = $items->where("price","=", request()->get("price"));
			}

			if(request()->has("time") && request()->get("time") != ""){
				$items = $items->where("time","=", request()->get("time"));
			}



        $items = $items->paginate(env('PAGINATE'));
        return view('website.serveries.index' , compact('items'));
     }

     public function show($id = null){
         return $this->createOrEdit('website.serveries.edit' , $id);
     }

     public function store(AddRequestServeries $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('serveries');
     }

     public function update($id , UpdateRequestServeries $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }

     public function getById($id){
         $fields = $this->model->findOrFail($id);
         return $this->createOrEdit('website.serveries.show' , $id , ['fields' =>  $fields]);
     }

     public function destroy($id){
         return $this->deleteItem($id , 'serveries')->with('sucess' , 'Done Delete Serveries From system');
     }


}
