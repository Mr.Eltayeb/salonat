<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Salons;
use App\Application\Transformers\SalonsTransformers;
use App\Application\Requests\Website\Salons\ApiAddRequestSalons;
use App\Application\Requests\Website\Salons\ApiUpdateRequestSalons;

class SalonsApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Salons $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }

    public function add(ApiAddRequestSalons $validation){
         return $this->addItem($validation);
    }

    public function getById($id, $lang = "en"){
$data = Salons::with(['serveries', 'staff', 'user'])->where('id', $id)->get();
        if ($data) {
            return response(apiReturn(SalonsTransformers::transform($data)), 200);
        }
        return response(apiReturn('null', '', 'No Data Found'), 200);    
    }

    public function index($limit = 10, $offset = 0, $lang = "en")
    {
        // $data = $this->model->limit($limit)->offset($offset)->get();
        $data = Salons::all();
        if ($data) {
            return response(apiReturn(SalonsTransformers::transform($data)), 200);
        }
        return response(apiReturn('', '', 'No Data Found'), 200);
    }

    public function update($id , ApiUpdateRequestSalons $validation){
        return $this->updateItem($id , $validation);
    }


    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(SalonsTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(SalonsTransformers::transform($data) + $paginate), $status_code);
    }


    public function salons($lang = "en"){
        $data = Salons::all();
                if ($data) {
                    return response()->json(['data' =>$data], 200);
                }
                return response(apiReturn('null', '', 'No Data Found'), 200);    
            }
}
