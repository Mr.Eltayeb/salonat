<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Testerd;
use App\Application\Transformers\TesterdTransformers;
use App\Application\Requests\Website\Testerd\ApiAddRequestTesterd;
use App\Application\Requests\Website\Testerd\ApiUpdateRequestTesterd;

class TesterdApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Testerd $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }

    public function getById($id, $lang = "en")
    {

        $data = $this->model->with('testerdrate')->find($id);
        if ($data) {
            return response(apiReturn(TesterdTransformers::transform($data)), 200);
        }
        return response(apiReturn('', '', 'No Data Found'), 200);
    }

    public function add(ApiAddRequestTesterd $validation){
         return $this->addItem($validation);
    }

    public function update($id , ApiUpdateRequestTesterd $validation){
        return $this->updateItem($id , $validation);
    }

    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(TesterdTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(TesterdTransformers::transform($data) + $paginate), $status_code);
    }

}
