<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Shoprequest;
use App\Application\Transformers\ShoprequestTransformers;
use App\Application\Requests\Website\Shoprequest\ApiAddRequestShoprequest;
use App\Application\Requests\Website\Shoprequest\ApiUpdateRequestShoprequest;

class ShoprequestApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Shoprequest $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }

    public function add(ApiAddRequestShoprequest $validation){
         return $this->addItem($validation);
    }

    public function update($id , ApiUpdateRequestShoprequest $validation){
        return $this->updateItem($id , $validation);
    }

    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(ShoprequestTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(ShoprequestTransformers::transform($data) + $paginate), $status_code);
    }

}
