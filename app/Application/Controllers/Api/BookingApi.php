<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Booking;
use App\Application\Model\Salons;
//use App\Application\Model\User;
use App\Application\Model\User;
use App\Application\Requests\Website\Booking\ApiUpdateStatus;
use App\Notifications\NewBooking;
use HttpException;
use HttpRequest;
use Illuminate\Http\Request;
use DB;
use Notification;
use Pusher\Pusher;
//use Pusher\Laravel\Facades\Pusher;
use Illuminate\Support\Facades\Validator;
use App\Application\Transformers\BookingTransformers;
use App\Application\Requests\Website\Booking\ApiAddRequestBooking;
use App\Application\Requests\Website\Booking\ApiUpdateRequestBooking;

class BookingApi extends Controller
{

    protected $request;
    protected $model;

    public function __construct(Booking $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
        /// send header Authorization Bearer token
//        $this->middleware('authApi')->only('index', 'add', 'update', 'getById');
    }

    public function index($limit = 10, $offset = 0, $lang = "en")
    {

        $data = $this->model->limit($limit)->offset($offset)->get();
        if ($data) {
            return response(apiReturn(BookingTransformers::transform($data)), 200);
        }
        return response(apiReturn('null', '', 'No Data Found'), 200);
    }

    public function getById($id, $lang = "en")
    {
        $data = $this->model->find($id);
        if ($data) {
            return response(apiReturn(BookingTransformers::transform($data)), 200);
        }
        return response(apiReturn($data, 'false', 'No Data Found'), 200);
    }

    public function getByStaffId($id, $lang = "en")
    {
        $data = Booking::where('staff_id', $id)->get();
        if ($data) {
            return response(apiReturn(BookingTransformers::transform($data)), 200);
        }
        return response(apiReturn('null', '', 'No Data Found'), 200);
    }

    public function getByUser($id, $lang = "en")
    {
        $data = Booking::with(['salons', 'serveries', 'staff'])
            ->where('user_id', $id)
            ->orderBy('updated_at', 'desc')
            ->get();
        if ($data) {
            return response(apiReturn(BookingTransformers::transform($data)), 200);
        }
        return response(apiReturn('null', '', 'No Data Found'), 200);
    }

    public function getByAdmin($adminID, $lang = "en")
    {
        $isAdmin = Salons::where('user_id', $adminID)->pluck('id');
        foreach ($isAdmin as $item) {
            $data = $this->model->where('salons_id', $item)
                ->orderBy('updated_at', 'desc')
                ->get();
            return response(apiReturn(BookingTransformers::transform($data)), 200);
        }
        return response(apiReturn('null', '', 'No Data Found'), 200);
    }


    public function add(ApiAddRequestBooking $validation)
    {
        $manUrl = "https://mazinhost.com/sms/API/?action=compose&username=sms_abubaker&api_key=a37467bf222f4eb891f1702e42da56bb:TQ6AFYBCIVbJ94fLghLSh7ZMYffkvAsI&sender=AlrynSchool&to=";
        $urlMsessage = "&message=";

        $request = $this->checkRequestType();
        $not = $this->request;
        $v = Validator::make($this->request->all(), $validation->rules());
        if ($v->fails()) {
            return response(apiReturn($v->errors(), 'error', ''), 200);
        }
//
//        idea
        $voo = Booking::where("staff_id", $this->request->staff_id)
            ->where("salons_id", $this->request->salons_id)
            ->whereTime('time', '=', $this->request->time)
            ->whereDate('date', $this->request->date)->count();
        if ($voo == 0) {
//        end idea
            $bookings = new Booking ();
            if ($booking = $this->model->create(transformArray(checkApiHaveImage($request)))) {
                $user = Salons::where('user_id', $this->request->salon_id)->first();
                $message = 'you have new Booking request at ' . $this->request->time . ' & ' . $this->request->date;

                $sendPusher = new Pusher("4fcc24e9773d0cf9a2aa", "e69ca5704b1e5bf29213", 542717, array('cluster' => "eu"));
                $sendPusher->trigger('my-channel', 'my-event', ['message' => $message]);
                // Notification::send($user, new NewBooking($bookings));

                $user = User::find($this->request->user_id);
                $number = ltrim($user->contact, '0');
                $username = $user->name;
                $mesassg = $username . "سوف يتم معالجت طلبكم";

                $fullUrl = $manUrl . "249" . $number . $urlMsessage . $mesassg;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_URL,
                    $fullUrl
                );
                $content = curl_exec($ch);

                return response(apiReturn(BookingTransformers::transform($booking)), 200);
            }

        } else {
            return response(apiReturn('', 'error', $v->errors()), 200);

        }
    }

    public function update($id, ApiUpdateRequestBooking $validation)
    {
        $request = $this->checkRequestType();
        $v = Validator::make($this->request->all(), $validation->rules());
        if ($v->fails()) {
            return response(apiReturn('', 'error', $v->errors()), 200);
        }
        $data = $this->model->find($id)->update(transformArray(checkApiHaveImage($request)));
        return response(apiReturn($data), 200);
    }

    public function updateStatus($id, ApiUpdateStatus $validation)
    {

//https://mazinhost.com/sms/API/?action=compose&username=sms_abubaker&api_key=a37467bf222f4eb891f1702e42da56bb:TQ6AFYBCIVbJ94fLghLSh7ZMYffkvAsI&sender=AlrynSchool&to=249917321783&message=";
        $manUrl = "https://mazinhost.com/sms/API/?action=compose&username=sms_abubaker&api_key=a37467bf222f4eb891f1702e42da56bb:TQ6AFYBCIVbJ94fLghLSh7ZMYffkvAsI&sender=AlrynSchool&to=";
        $urlMsessage = "&message=";
        $request = $this->checkRequestType();
        $v = Validator::make($this->request->all(), $validation->rules());
        if ($v->fails()) {
            return response(apiReturn('', 'error', $v->errors()), 200);
        }
        foreach ($request as $request) {
            $request = $request;
        }
//        $helps = Booking::where('id', $id)
//            ->user
        $user = Booking::find($id)
            ->user()->get();

        foreach ($user as $phoneNumber) {
        }
        $number = ltrim($phoneNumber->contact, '0');
        $username = $phoneNumber->name;
        $mesassg = $username . " تم معالجت طلبكم " . " رقم " . $id . " شكرًا ";

        $fullUrl = $manUrl . "249" . $number . $urlMsessage . $mesassg;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,
            $fullUrl
        );
        $content = curl_exec($ch);
        $data = Booking::find($id)->update(['status' => $request]);

        return response(apiReturn([$data, $content]), 200);
    }

    public function delete($id)
    {
        $data = $this->model->find($id)->delete();
        return response(apiReturn($data), 200);
    }

    protected function checkRequestType()
    {
        return $this->request->getContentType() == "json" ? extractJsonInfo($this->request->getContent()) : $this->request->all();
    }

}
