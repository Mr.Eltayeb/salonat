<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Shopitem;
use App\Application\Transformers\ShopitemTransformers;
use App\Application\Requests\Website\Shopitem\ApiAddRequestShopitem;
use App\Application\Requests\Website\Shopitem\ApiUpdateRequestShopitem;

class ShopitemApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Shopitem $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }

    public function add(ApiAddRequestShopitem $validation){
         return $this->addItem($validation);
    }

    public function update($id , ApiUpdateRequestShopitem $validation){
        return $this->updateItem($id , $validation);
    }

    protected function checkLanguageBeforeReturn($data , $status_code = 200, $paginate = [])
    {
       if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(ShopitemTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(ShopitemTransformers::transform($data) + $paginate), $status_code);
    }

}
