<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Staff;
use App\Application\Transformers\StaffTransformers;
use App\Application\Requests\Website\Staff\ApiAddRequestStaff;
use App\Application\Requests\Website\Staff\ApiUpdateRequestStaff;

class StaffApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Staff $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }


    public function index($limit = 10, $offset = 0, $lang = "en")
    {
        $data = $this->model->limit($limit)->offset($offset)->get();
        if ($data) {
            return response(apiReturn(StaffTransformers::transform($data)), 200);
        }
        return response(apiReturn('', '', 'No Data Found'), 200);
    }

    public function getById($id, $lang = "en")
    {
//        $data = $this->model->find($id);
        $data = Staff::where('user_id', $id)->get();
        if ($data) {
            return response(apiReturn(StaffTransformers::transform($data)), 200);
        }
        return response(apiReturn('', '', 'No Data Found'), 200);
    }


    public function add(ApiAddRequestStaff $validation)
    {
        return $this->addItem($validation);
    }

    public function update($id, ApiUpdateRequestStaff $validation)
    {
        return $this->updateItem($id, $validation);
    }

    protected function checkLanguageBeforeReturn($data, $status_code = 200, $paginate = [])
    {
        if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(StaffTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(StaffTransformers::transform($data) + $paginate), $status_code);
    }
    public function staffs($lang = "en")
    {
//        $data = $this->model->find($id);
        $data = Staff::all();
        if ($data) {
            return response(apiReturn(StaffTransformers::transform($data)), 200);
        }
        return response(apiReturn('', '', 'No Data Found'), 200);
    }

}
