<?php

namespace App\Application\Controllers\Api;


use App\Application\Controllers\Controller;
use App\Application\Model\Serveries;
use App\Application\Transformers\ServeriesTransformers;
use App\Application\Requests\Website\Serveries\ApiAddRequestServeries;
use App\Application\Requests\Website\Serveries\ApiUpdateRequestServeries;

class ServeriesApi extends Controller
{
    use ApiTrait;
    protected $model;

    public function __construct(Serveries $model)
    {
        $this->model = $model;
        /// send header Authorization Bearer token
        /// $this->middleware('authApi')->only();
    }

    public function index($limit = 10, $offset = 0, $lang = "en")
    {
        $data = $this->model->limit($limit)->offset($offset)->get();
        if ($data) {
            return response(apiReturn(ServeriesTransformers::transform($data)), 200);
        }
        return response(apiReturn('', '', 'No Data Found'), 200);
    }

    public function getById($id, $lang = "en"){
        $data = Serveries::where('id', $id)->get();
                if ($data) {
                    return response(apiReturn(ServeriesTransformers::transform($data)), 200);
                }
                return response(apiReturn('null', '', 'No Data Found'), 200);    
            }


    public function add(ApiAddRequestServeries $validation)
    {
        return $this->addItem($validation);
    }

    public function update($id, ApiUpdateRequestServeries $validation)
    {
        return $this->updateItem($id, $validation);
    }

    protected function checkLanguageBeforeReturn($data, $status_code = 200, $paginate = [])
    {
        if (request()->has('lang') && request()->get('lang') == 'ar') {
            return response(apiReturn(ServeriesTransformers::transformAr($data) + $paginate), $status_code);
        }
        return response(apiReturn(ServeriesTransformers::transform($data) + $paginate), $status_code);
    }

}
