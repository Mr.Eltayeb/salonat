<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Staff\AddRequestStaff;
use App\Application\Requests\Admin\Staff\UpdateRequestStaff;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\StaffsDataTable;
use App\Application\Model\Staff;
use Yajra\Datatables\Request;
use Alert;

class StaffController extends AbstractController
{
    public function __construct(Staff $model)
    {
        parent::__construct($model);
    }

    public function index(StaffsDataTable $dataTable){
        return $dataTable->render('admin.staff.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.staff.edit' , $id);
    }

     public function store(AddRequestStaff $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/staff');
     }

     public function update($id , UpdateRequestStaff $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.staff.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/staff')->with('sucess' , 'Done Delete staff From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/staff')->with('sucess' , 'Done Delete staff From system');
    }

}
