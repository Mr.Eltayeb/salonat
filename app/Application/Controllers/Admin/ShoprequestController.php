<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Shoprequest\AddRequestShoprequest;
use App\Application\Requests\Admin\Shoprequest\UpdateRequestShoprequest;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\ShoprequestsDataTable;
use App\Application\Model\Shoprequest;
use Yajra\Datatables\Request;
use Alert;

class ShoprequestController extends AbstractController
{
    public function __construct(Shoprequest $model)
    {
        parent::__construct($model);
    }

    public function index(ShoprequestsDataTable $dataTable){
        return $dataTable->render('admin.shoprequest.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.shoprequest.edit' , $id);
    }

     public function store(AddRequestShoprequest $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/shoprequest');
     }

     public function update($id , UpdateRequestShoprequest $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.shoprequest.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/shoprequest')->with('sucess' , 'Done Delete shoprequest From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/shoprequest')->with('sucess' , 'Done Delete shoprequest From system');
    }

}
