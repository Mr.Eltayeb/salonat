<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Salons\AddRequestSalons;
use App\Application\Requests\Admin\Salons\UpdateRequestSalons;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\SalonssDataTable;
use App\Application\Model\Salons;
use Yajra\Datatables\Request;
use Alert;

class SalonsController extends AbstractController
{
    public function __construct(Salons $model)
    {
        parent::__construct($model);
    }

    public function index(SalonssDataTable $dataTable){
        return $dataTable->render('admin.salons.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.salons.edit' , $id);
    }

     public function store(AddRequestSalons $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/salons');
     }

     public function update($id , UpdateRequestSalons $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.salons.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/salons')->with('sucess' , 'Done Delete salons From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/salons')->with('sucess' , 'Done Delete salons From system');
    }

}
