<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Shopitem\AddRequestShopitem;
use App\Application\Requests\Admin\Shopitem\UpdateRequestShopitem;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\ShopitemsDataTable;
use App\Application\Model\Shopitem;
use Yajra\Datatables\Request;
use Alert;

class ShopitemController extends AbstractController
{
    public function __construct(Shopitem $model)
    {
        parent::__construct($model);
    }

    public function index(ShopitemsDataTable $dataTable){
        return $dataTable->render('admin.shopitem.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.shopitem.edit' , $id);
    }

     public function store(AddRequestShopitem $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/shopitem');
     }

     public function update($id , UpdateRequestShopitem $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.shopitem.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/shopitem')->with('sucess' , 'Done Delete shopitem From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/shopitem')->with('sucess' , 'Done Delete shopitem From system');
    }

}
