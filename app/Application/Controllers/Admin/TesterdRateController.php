<?php

namespace App\Application\Controllers\Admin;

use \App\Application\Requests\Admin\TesterdRate\AddRequestTesterdRate;
use \App\Application\Requests\Admin\TesterdRate\UpdateRequestTesterdRate;
use App\Application\Controllers\AbstractController;
use App\Application\Model\TesterdRate;
use App\Application\Model\Testerd;
use Yajra\Datatables\Request;
use Alert;

class TesterdRateController extends AbstractController
{

   public function __construct(TesterdRate $model , Testerd $parent)
    {
        parent::__construct($model);
        $this->parent = $parent;
    }

    public function addRate($id , AddRequestTesterdRate $request){
        if($this->checkIfUserRateBefore($id)){
            alert()->error(trans('admin.You have rate this before') , trans('admin.error'));
            return redirect('admin/testerd/'.$id.'/view');
        }
        $array = [
            'rate' => $request->rate,
            'user_id' => auth()->user()->id,
            'testerd_id' => $id
        ];
        $this->model->create($array);
        return redirect('admin/testerd/'.$id.'/view');
    }

    public function checkIfUserRateBefore($id){
        $item = $this->model->where('id' , $id)->where('user_id' , auth()->user()->id)->first();
        return $item ? true : false;
    }

}