<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Booking\AddRequestBooking;
use App\Application\Requests\Admin\Booking\UpdateRequestBooking;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\BookingsDataTable;
use App\Application\Model\Booking;
use Yajra\Datatables\Request;
use Alert;

class BookingController extends AbstractController
{
    public function __construct(Booking $model)
    {
        parent::__construct($model);
    }

    public function index(BookingsDataTable $dataTable){
        return $dataTable->render('admin.booking.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.booking.edit' , $id);
    }

     public function store(AddRequestBooking $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/booking');
     }

     public function update($id , UpdateRequestBooking $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.booking.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/booking')->with('sucess' , 'Done Delete booking From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/booking')->with('sucess' , 'Done Delete booking From system');
    }

}
