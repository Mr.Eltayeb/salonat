<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Testerd\AddRequestTesterd;
use App\Application\Requests\Admin\Testerd\UpdateRequestTesterd;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\TesterdsDataTable;
use App\Application\Model\Testerd;
use Yajra\Datatables\Request;
use Alert;

class TesterdController extends AbstractController
{
    public function __construct(Testerd $model)
    {
        parent::__construct($model);
    }

    public function index(TesterdsDataTable $dataTable){
        return $dataTable->render('admin.testerd.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.testerd.edit' , $id);
    }

     public function store(AddRequestTesterd $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/testerd');
     }

     public function update($id , UpdateRequestTesterd $request){
          if ($request->has("oldFiles_image") && $request->oldFiles_image != "") {
                                        $oldImage_image = $request->oldFiles_image;
                                        $request->request->remove("oldFiles_image");
                                    } else {
                                        $oldImage_image = json_encode([]);
                                    }
$item = $this->storeOrUpdate($request, $id, true);
if ($item) {
                                    $image = json_decode($item->image) ?? [];
                                    $newIamge = json_decode($oldImage_image) ?? [];
                                    $item_image = array_unique(array_merge($image, $newIamge));
                                    $item->image = json_encode($item_image);
                                    $item->save();
                                }
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.testerd.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/testerd')->with('sucess' , 'Done Delete testerd From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/testerd')->with('sucess' , 'Done Delete testerd From system');
    }

}
