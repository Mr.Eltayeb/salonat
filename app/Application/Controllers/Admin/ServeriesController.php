<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Serveries\AddRequestServeries;
use App\Application\Requests\Admin\Serveries\UpdateRequestServeries;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\ServeriessDataTable;
use App\Application\Model\Serveries;
use Yajra\Datatables\Request;
use Alert;

class ServeriesController extends AbstractController
{
    public function __construct(Serveries $model)
    {
        parent::__construct($model);
    }

    public function index(ServeriessDataTable $dataTable){
        return $dataTable->render('admin.serveries.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.serveries.edit' , $id);
    }

     public function store(AddRequestServeries $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/serveries');
     }

     public function update($id , UpdateRequestServeries $request){
          $item = $this->storeOrUpdate($request, $id, true);
return redirect()->back();

     }


    public function getById($id){
        $fields = $this->model->findOrFail($id);
        return $this->createOrEdit('admin.serveries.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/serveries')->with('sucess' , 'Done Delete serveries From system');
    }

    public function pluck(\Illuminate\Http\Request $request){
        return $this->deleteItem($request->id , 'admin/serveries')->with('sucess' , 'Done Delete serveries From system');
    }

}
