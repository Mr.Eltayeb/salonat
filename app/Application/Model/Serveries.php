<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Serveries extends Model
{
    public $table = "serveries";

    public function booking()
    {
        return $this->hasOne(Booking::class, "serveries_id");
    }

    public function salons()
    {
        return $this->belongsTo(Salons::class, "salons_id");
    }

    protected $fillable = [
        'salons_id',
        'name', 'type', 'price', 'time'
    ];
}
