<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class TesterdRate extends Model
{
   public $table = "testerdrate";
   public function user(){
		return $this->belongsTo(User::class, "user_id");
		}
  public function testerd(){
  return $this->belongsTo(Testerd::class, "testerd_id");
  }
     protected $fillable = [
        '','user_id','testerd_id','rate'
   ];
  }
