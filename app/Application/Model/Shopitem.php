<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Shopitem extends Model
{
   public $table = "shopitem";
  public function shoprequest(){
		return $this->hasMany(Shoprequest::class, "shopitem_id");
		}
     protected $fillable = [
        'name','price','quantity','description','image','image_two','image_three'
   ];
  }
