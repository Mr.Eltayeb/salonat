<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Salons extends Model
{
    public $table = "salons";

    public function serveries()
    {
        return $this->hasMany(Serveries::class, "salons_id");
    }

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function booking()
    {
        return $this->hasMany(Booking::class, "salons_id");
    }

    public function staff()
    {
        return $this->hasMany(Staff::class, "salons_id");
    }

    protected $fillable = [
        'user_id',
        'name',
        'details',
        'image',
        'lat',
        'lng',
        'Address',
        'photo',
        'photo_two',
        'photo_three',
        'logo'
    ];
}
