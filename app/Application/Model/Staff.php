<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Staff extends Model
{
   public $table = "staff";
   public function booking(){
		return $this->hasMany(Booking::class, "staff_id");
		}
   public function user(){
  return $this->belongsTo(User::class, "user_id");
  }
   public function salons(){
  return $this->belongsTo(Salons::class, "salons_id");
  }
     protected $fillable = [
     'user_id',
   'salons_id',
        'name','details','image'
   ];
  }
