<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Shoprequest extends Model
{
   public $table = "shoprequest";
   public function user(){
		return $this->belongsTo(User::class, "user_id");
		}
   public function shopitem(){
  return $this->belongsTo(Shopitem::class, "shopitem_id");
  }
     protected $fillable = [
     'user_id',
   'shopitem_id',
        'item_quantity','item_totel_price'
   ];
  }
