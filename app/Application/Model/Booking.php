<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public $table = "booking";

    public function staff()
    {
        return $this->belongsTo(Staff::class, "staff_id");
    }

    public function serveries()
    {
        return $this->belongsTo(Serveries::class, "serveries_id");
    }

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function salons()
    {
        return $this->belongsTo(Salons::class, "salons_id");
    }

    protected $fillable = [
        'staff_id',
        'serveries_id',
        'user_id',
        'salons_id',
        'time',
        'date',
        'feedback',
        'status'
    ];
}
