<?php

namespace App\Application\DataTables;

use App\Application\Model\Shopitem;
use Yajra\Datatables\Services\DataTable;

class ShopitemsDataTable extends DataTable
{
    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
             ->eloquent($this->query())
              ->addColumn('id', 'admin.shopitem.buttons.id')
             ->addColumn('edit', 'admin.shopitem.buttons.edit')
             ->addColumn('delete', 'admin.shopitem.buttons.delete')
             ->addColumn('view', 'admin.shopitem.buttons.view')
             /*->addColumn('name', 'admin.shopitem.buttons.langcol')*/
             ->make(true);
    }
    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Shopitem::query();

        if(request()->has('from') && request()->get('from') != ''){
            $query = $query->whereDate('created_at' , '>=' , request()->get('from'));
        }

        if(request()->has('to') && request()->get('to') != ''){
            $query = $query->whereDate('created_at' , '<=' , request()->get('to'));
        }

		if(request()->has("name") && request()->get("name") != ""){
				$query = $query->where("name","=", request()->get("name"));
		}

		if(request()->has("price") && request()->get("price") != ""){
				$query = $query->where("price","=", request()->get("price"));
		}

		if(request()->has("quantity") && request()->get("quantity") != ""){
				$query = $query->where("quantity","=", request()->get("quantity"));
		}

		if(request()->has("description") && request()->get("description") != ""){
				$query = $query->where("description","=", request()->get("description"));
		}

		if(request()->has("image_two") && request()->get("image_two") != ""){
				$query = $query->where("image_two","=", request()->get("image_two"));
		}

		if(request()->has("image_three") && request()->get("image_three") != ""){
				$query = $query->where("image_three","=", request()->get("image_three"));
		}



        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->parameters(dataTableConfig());
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
              [
                  'name' => "id",
                  'data' => 'id',
                  'title' => trans('curd.id'),
             ],
			[
                'name' => 'name',
                'data' => 'name',
                'title' => "name",
                
                ],
             [
                  'name' => 'view',
                  'data' => 'view',
                  'title' => trans('curd.view'),
                  'exportable' => false,
                  'printable' => false,
                  'searchable' => false,
                  'orderable' => false,
             ],
             [
                  'name' => 'edit',
                  'data' => 'edit',
                  'title' =>  trans('curd.edit'),
                  'exportable' => false,
                  'printable' => false,
                  'searchable' => false,
                  'orderable' => false,
             ],
             [
                   'name' => 'delete',
                   'data' => 'delete',
                   'title' => trans('curd.delete'),
                   'exportable' => false,
                   'printable' => false,
                   'searchable' => false,
                   'orderable' => false,
             ],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Shopitemdatatables_' . time();
    }
}