<?php

namespace App\Notifications;

use App\Application\Model\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pusher\Pusher;


//use Illuminate\Notifications\Messages\MailMessage;

class NewBooking extends Notification
{
    use Queueable;

    protected $booking;
    // protected $sendPusher;

    public function __construct(Booking $booking )
    {
        $this->booking = $booking;
        // $sendPusher = new Pusher( "4fcc24e9773d0cf9a2aa", "e69ca5704b1e5bf29213", 542717,  array('cluster' => "eu")  );

    }

    public function via($notifiable)
    {
        return ['database'];
    }


//    public function toDatabase($notifiable)
//    {
//        return [
//            'data' => 'you have new Booking' . 'request by ' . auth()->user()->name . 'Time' . $this->booking->time
//        ];
//    }

    function toArray($notifiable)
    {
        // $message= 'you have new Booking request at ' .$this->booking->time. ' & '. $this->booking->date;

        return [
            'data' => ''
            // $sendPusher->trigger('my-channel', 'my-event', ['message' => $message])
        ];
    }
}
